# CAUTION: EXPERIMENTAL VERSION
May not be perfectly safe, or may have unintended features.<br>
<img src="https://api.visitorbadge.io/api/visitors?path=gitlab.ncfoss.hr&countColor=%2337d67a&style=flat-square&labelStyle=upper"><br>

## This is free and unencumbered software released into the public domain.
No rights reserved.

## Requires files to work
Only the `src/.php`, the `src/mimes.php` is optional.<br>

## Recommended
Take a look at examples provided in zip file `src/optional-examples.zip` or similar filename.<br>

## Installation
Upload `src/.php` to server and run it in browser, and it **should** install itself.

## Plans
No plans for now; **subject to change**.<br>

## License
This project is licensed under Unlicense, and is dedicated to public domain.<br>
