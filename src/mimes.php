<?php
$MimeListGlobal = array (
'application/andrew-inset' =>
array (
0 => 'ez',
),
'ez' => 'application/andrew-inset',
'application/applixware' =>
array (
0 => 'aw',
),
'aw' => 'application/applixware',
'application/atom+xml' =>
array (
0 => 'atom',
),
'atom' => 'application/atom+xml',
'application/atomcat+xml' =>
array (
0 => 'atomcat',
),
'atomcat' => 'application/atomcat+xml',
'application/atomsvc+xml' =>
array (
0 => 'atomsvc',
),
'atomsvc' => 'application/atomsvc+xml',
'application/ccxml+xml' =>
array (
0 => 'ccxml',
),
'ccxml' => 'application/ccxml+xml',
'application/cdmi-capability' =>
array (
0 => 'cdmia',
),
'cdmia' => 'application/cdmi-capability',
'application/cdmi-container' =>
array (
0 => 'cdmic',
),
'cdmic' => 'application/cdmi-container',
'application/cdmi-domain' =>
array (
0 => 'cdmid',
),
'cdmid' => 'application/cdmi-domain',
'application/cdmi-object' =>
array (
0 => 'cdmio',
),
'cdmio' => 'application/cdmi-object',
'application/cdmi-queue' =>
array (
0 => 'cdmiq',
),
'cdmiq' => 'application/cdmi-queue',
'application/cu-seeme' =>
array (
0 => 'cu',
),
'cu' => 'application/cu-seeme',
'application/davmount+xml' =>
array (
0 => 'davmount',
),
'davmount' => 'application/davmount+xml',
'application/docbook+xml' =>
array (
0 => 'dbk',
),
'dbk' => 'application/docbook+xml',
'application/dssc+der' =>
array (
0 => 'dssc',
),
'dssc' => 'application/dssc+der',
'application/dssc+xml' =>
array (
0 => 'xdssc',
),
'xdssc' => 'application/dssc+xml',
'application/ecmascript' =>
array (
0 => 'ecma',
),
'ecma' => 'application/ecmascript',
'application/emma+xml' =>
array (
0 => 'emma',
),
'emma' => 'application/emma+xml',
'application/epub+zip' =>
array (
0 => 'epub',
),
'epub' => 'application/epub+zip',
'application/exi' =>
array (
0 => 'exi',
),
'exi' => 'application/exi',
'application/font-tdpfr' =>
array (
0 => 'pfr',
),
'pfr' => 'application/font-tdpfr',
'application/gml+xml' =>
array (
0 => 'gml',
),
'gml' => 'application/gml+xml',
'application/gpx+xml' =>
array (
0 => 'gpx',
),
'gpx' => 'application/gpx+xml',
'application/gxf' =>
array (
0 => 'gxf',
),
'gxf' => 'application/gxf',
'application/hyperstudio' =>
array (
0 => 'stk',
),
'stk' => 'application/hyperstudio',
'application/inkml+xml' =>
array (
0 => 'ink',
1 => 'inkml',
),
'ink' => 'application/inkml+xml',
'inkml' => 'application/inkml+xml',
'application/ipfix' =>
array (
0 => 'ipfix',
),
'ipfix' => 'application/ipfix',
'application/java-archive' =>
array (
0 => 'jar',
),
'jar' => 'application/java-archive',
'application/java-serialized-object' =>
array (
0 => 'ser',
),
'ser' => 'application/java-serialized-object',
'application/java-vm' =>
array (
0 => 'class',
),
'class' => 'application/java-vm',
'application/json' =>
array (
0 => 'json',
),
'json' => 'application/json',
'application/jsonml+json' =>
array (
0 => 'jsonml',
),
'jsonml' => 'application/jsonml+json',
'application/lost+xml' =>
array (
0 => 'lostxml',
),
'lostxml' => 'application/lost+xml',
'application/mac-binhex40' =>
array (
0 => 'hqx',
),
'hqx' => 'application/mac-binhex40',
'application/mac-compactpro' =>
array (
0 => 'cpt',
),
'cpt' => 'application/mac-compactpro',
'application/mads+xml' =>
array (
0 => 'mads',
),
'mads' => 'application/mads+xml',
'application/marc' =>
array (
0 => 'mrc',
),
'mrc' => 'application/marc',
'application/marcxml+xml' =>
array (
0 => 'mrcx',
),
'mrcx' => 'application/marcxml+xml',
'application/mathematica' =>
array (
0 => 'ma',
1 => 'nb',
2 => 'mb',
),
'ma' => 'application/mathematica',
'nb' => 'application/mathematica',
'mb' => 'application/mathematica',
'application/mathml+xml' =>
array (
0 => 'mathml',
),
'mathml' => 'application/mathml+xml',
'application/mbox' =>
array (
0 => 'mbox',
),
'mbox' => 'application/mbox',
'application/mediaservercontrol+xml' =>
array (
0 => 'mscml',
),
'mscml' => 'application/mediaservercontrol+xml',
'application/metalink+xml' =>
array (
0 => 'metalink',
),
'metalink' => 'application/metalink+xml',
'application/metalink4+xml' =>
array (
0 => 'meta4',
),
'meta4' => 'application/metalink4+xml',
'application/mets+xml' =>
array (
0 => 'mets',
),
'mets' => 'application/mets+xml',
'application/mods+xml' =>
array (
0 => 'mods',
),
'mods' => 'application/mods+xml',
'application/mp21' =>
array (
0 => 'm21',
1 => 'mp21',
),
'm21' => 'application/mp21',
'mp21' => 'application/mp21',
'application/mp4' =>
array (
0 => 'mp4s',
),
'mp4s' => 'application/mp4',
'application/msword' =>
array (
0 => 'doc',
1 => 'dot',
),
'doc' => 'application/msword',
'dot' => 'application/msword',
'application/mxf' =>
array (
0 => 'mxf',
),
'mxf' => 'application/mxf',
'application/octet-stream' =>
array (
0 => 'bin',
1 => 'dms',
2 => 'lrf',
3 => 'mar',
4 => 'so',
5 => 'dist',
6 => 'distz',
7 => 'pkg',
8 => 'bpk',
9 => 'dump',
10 => 'elc',
11 => 'deploy',
),
'bin' => 'application/octet-stream',
'dms' => 'application/octet-stream',
'lrf' => 'application/octet-stream',
'mar' => 'application/octet-stream',
'so' => 'application/octet-stream',
'dist' => 'application/octet-stream',
'distz' => 'application/octet-stream',
'pkg' => 'application/octet-stream',
'bpk' => 'application/octet-stream',
'dump' => 'application/octet-stream',
'elc' => 'application/octet-stream',
'deploy' => 'application/octet-stream',
'application/oda' =>
array (
0 => 'oda',
),
'oda' => 'application/oda',
'application/oebps-package+xml' =>
array (
0 => 'opf',
),
'opf' => 'application/oebps-package+xml',
'application/ogg' =>
array (
0 => 'ogx',
),
'ogx' => 'application/ogg',
'application/omdoc+xml' =>
array (
0 => 'omdoc',
),
'omdoc' => 'application/omdoc+xml',
'application/onenote' =>
array (
0 => 'onetoc',
1 => 'onetoc2',
2 => 'onetmp',
3 => 'onepkg',
),
'onetoc' => 'application/onenote',
'onetoc2' => 'application/onenote',
'onetmp' => 'application/onenote',
'onepkg' => 'application/onenote',
'application/oxps' =>
array (
0 => 'oxps',
),
'oxps' => 'application/oxps',
'application/patch-ops-error+xml' =>
array (
0 => 'xer',
),
'xer' => 'application/patch-ops-error+xml',
'application/pdf' =>
array (
0 => 'pdf',
),
'pdf' => 'application/pdf',
'application/pgp-encrypted' =>
array (
0 => 'pgp',
),
'pgp' => 'application/pgp-encrypted',
'application/pgp-signature' =>
array (
0 => 'asc',
1 => 'sig',
),
'asc' => 'application/pgp-signature',
'sig' => 'application/pgp-signature',
'application/pics-rules' =>
array (
0 => 'prf',
),
'prf' => 'application/pics-rules',
'application/pkcs10' =>
array (
0 => 'p10',
),
'p10' => 'application/pkcs10',
'application/pkcs7-mime' =>
array (
0 => 'p7m',
1 => 'p7c',
),
'p7m' => 'application/pkcs7-mime',
'p7c' => 'application/pkcs7-mime',
'application/pkcs7-signature' =>
array (
0 => 'p7s',
),
'p7s' => 'application/pkcs7-signature',
'application/pkcs8' =>
array (
0 => 'p8',
),
'p8' => 'application/pkcs8',
'application/pkix-attr-cert' =>
array (
0 => 'ac',
),
'ac' => 'application/pkix-attr-cert',
'application/pkix-cert' =>
array (
0 => 'cer',
),
'cer' => 'application/pkix-cert',
'application/pkix-crl' =>
array (
0 => 'crl',
),
'crl' => 'application/pkix-crl',
'application/pkix-pkipath' =>
array (
0 => 'pkipath',
),
'pkipath' => 'application/pkix-pkipath',
'application/pkixcmp' =>
array (
0 => 'pki',
),
'pki' => 'application/pkixcmp',
'application/pls+xml' =>
array (
0 => 'pls',
),
'pls' => 'application/pls+xml',
'application/postscript' =>
array (
0 => 'ai',
1 => 'eps',
2 => 'ps',
),
'ai' => 'application/postscript',
'eps' => 'application/postscript',
'ps' => 'application/postscript',
'application/prs.cww' =>
array (
0 => 'cww',
),
'cww' => 'application/prs.cww',
'application/pskc+xml' =>
array (
0 => 'pskcxml',
),
'pskcxml' => 'application/pskc+xml',
'application/rdf+xml' =>
array (
0 => 'rdf',
),
'rdf' => 'application/rdf+xml',
'application/reginfo+xml' =>
array (
0 => 'rif',
),
'rif' => 'application/reginfo+xml',
'application/relax-ng-compact-syntax' =>
array (
0 => 'rnc',
),
'rnc' => 'application/relax-ng-compact-syntax',
'application/resource-lists+xml' =>
array (
0 => 'rl',
),
'rl' => 'application/resource-lists+xml',
'application/resource-lists-diff+xml' =>
array (
0 => 'rld',
),
'rld' => 'application/resource-lists-diff+xml',
'application/rls-services+xml' =>
array (
0 => 'rs',
),
'rs' => 'application/rls-services+xml',
'application/rpki-ghostbusters' =>
array (
0 => 'gbr',
),
'gbr' => 'application/rpki-ghostbusters',
'application/rpki-manifest' =>
array (
0 => 'mft',
),
'mft' => 'application/rpki-manifest',
'application/rpki-roa' =>
array (
0 => 'roa',
),
'roa' => 'application/rpki-roa',
'application/rsd+xml' =>
array (
0 => 'rsd',
),
'rsd' => 'application/rsd+xml',
'application/rss+xml' =>
array (
0 => 'rss',
),
'rss' => 'application/rss+xml',
'application/rtf' =>
array (
0 => 'rtf',
),
'rtf' => 'application/rtf',
'application/sbml+xml' =>
array (
0 => 'sbml',
),
'sbml' => 'application/sbml+xml',
'application/scvp-cv-request' =>
array (
0 => 'scq',
),
'scq' => 'application/scvp-cv-request',
'application/scvp-cv-response' =>
array (
0 => 'scs',
),
'scs' => 'application/scvp-cv-response',
'application/scvp-vp-request' =>
array (
0 => 'spq',
),
'spq' => 'application/scvp-vp-request',
'application/scvp-vp-response' =>
array (
0 => 'spp',
),
'spp' => 'application/scvp-vp-response',
'application/sdp' =>
array (
0 => 'sdp',
),
'sdp' => 'application/sdp',
'application/set-payment-initiation' =>
array (
0 => 'setpay',
),
'setpay' => 'application/set-payment-initiation',
'application/set-registration-initiation' =>
array (
0 => 'setreg',
),
'setreg' => 'application/set-registration-initiation',
'application/shf+xml' =>
array (
0 => 'shf',
),
'shf' => 'application/shf+xml',
'application/smil+xml' =>
array (
0 => 'smi',
1 => 'smil',
),
'smi' => 'application/smil+xml',
'smil' => 'application/smil+xml',
'application/sparql-query' =>
array (
0 => 'rq',
),
'rq' => 'application/sparql-query',
'application/sparql-results+xml' =>
array (
0 => 'srx',
),
'srx' => 'application/sparql-results+xml',
'application/srgs' =>
array (
0 => 'gram',
),
'gram' => 'application/srgs',
'application/srgs+xml' =>
array (
0 => 'grxml',
),
'grxml' => 'application/srgs+xml',
'application/sru+xml' =>
array (
0 => 'sru',
),
'sru' => 'application/sru+xml',
'application/ssdl+xml' =>
array (
0 => 'ssdl',
),
'ssdl' => 'application/ssdl+xml',
'application/ssml+xml' =>
array (
0 => 'ssml',
),
'ssml' => 'application/ssml+xml',
'application/tei+xml' =>
array (
0 => 'tei',
1 => 'teicorpus',
),
'tei' => 'application/tei+xml',
'teicorpus' => 'application/tei+xml',
'application/thraud+xml' =>
array (
0 => 'tfi',
),
'tfi' => 'application/thraud+xml',
'application/timestamped-data' =>
array (
0 => 'tsd',
),
'tsd' => 'application/timestamped-data',
'application/vnd.3gpp.pic-bw-large' =>
array (
0 => 'plb',
),
'plb' => 'application/vnd.3gpp.pic-bw-large',
'application/vnd.3gpp.pic-bw-small' =>
array (
0 => 'psb',
),
'psb' => 'application/vnd.3gpp.pic-bw-small',
'application/vnd.3gpp.pic-bw-var' =>
array (
0 => 'pvb',
),
'pvb' => 'application/vnd.3gpp.pic-bw-var',
'application/vnd.3gpp2.tcap' =>
array (
0 => 'tcap',
),
'tcap' => 'application/vnd.3gpp2.tcap',
'application/vnd.3m.post-it-notes' =>
array (
0 => 'pwn',
),
'pwn' => 'application/vnd.3m.post-it-notes',
'application/vnd.accpac.simply.aso' =>
array (
0 => 'aso',
),
'aso' => 'application/vnd.accpac.simply.aso',
'application/vnd.accpac.simply.imp' =>
array (
0 => 'imp',
),
'imp' => 'application/vnd.accpac.simply.imp',
'application/vnd.acucobol' =>
array (
0 => 'acu',
),
'acu' => 'application/vnd.acucobol',
'application/vnd.acucorp' =>
array (
0 => 'atc',
1 => 'acutc',
),
'atc' => 'application/vnd.acucorp',
'acutc' => 'application/vnd.acucorp',
'application/vnd.adobe.air-application-installer-package+zip' =>
array (
0 => 'air',
),
'air' => 'application/vnd.adobe.air-application-installer-package+zip',
'application/vnd.adobe.formscentral.fcdt' =>
array (
0 => 'fcdt',
),
'fcdt' => 'application/vnd.adobe.formscentral.fcdt',
'application/vnd.adobe.fxp' =>
array (
0 => 'fxp',
1 => 'fxpl',
),
'fxp' => 'application/vnd.adobe.fxp',
'fxpl' => 'application/vnd.adobe.fxp',
'application/vnd.adobe.xdp+xml' =>
array (
0 => 'xdp',
),
'xdp' => 'application/vnd.adobe.xdp+xml',
'application/vnd.adobe.xfdf' =>
array (
0 => 'xfdf',
),
'xfdf' => 'application/vnd.adobe.xfdf',
'application/vnd.ahead.space' =>
array (
0 => 'ahead',
),
'ahead' => 'application/vnd.ahead.space',
'application/vnd.airzip.filesecure.azf' =>
array (
0 => 'azf',
),
'azf' => 'application/vnd.airzip.filesecure.azf',
'application/vnd.airzip.filesecure.azs' =>
array (
0 => 'azs',
),
'azs' => 'application/vnd.airzip.filesecure.azs',
'application/vnd.amazon.ebook' =>
array (
0 => 'azw',
),
'azw' => 'application/vnd.amazon.ebook',
'application/vnd.americandynamics.acc' =>
array (
0 => 'acc',
),
'acc' => 'application/vnd.americandynamics.acc',
'application/vnd.amiga.ami' =>
array (
0 => 'ami',
),
'ami' => 'application/vnd.amiga.ami',
'application/vnd.android.package-archive' =>
array (
0 => 'apk',
),
'apk' => 'application/vnd.android.package-archive',
'application/vnd.anser-web-certificate-issue-initiation' =>
array (
0 => 'cii',
),
'cii' => 'application/vnd.anser-web-certificate-issue-initiation',
'application/vnd.anser-web-funds-transfer-initiation' =>
array (
0 => 'fti',
),
'fti' => 'application/vnd.anser-web-funds-transfer-initiation',
'application/vnd.antix.game-component' =>
array (
0 => 'atx',
),
'atx' => 'application/vnd.antix.game-component',
'application/vnd.apple.installer+xml' =>
array (
0 => 'mpkg',
),
'mpkg' => 'application/vnd.apple.installer+xml',
'application/vnd.apple.mpegurl' =>
array (
0 => 'm3u8',
),
'm3u8' => 'application/vnd.apple.mpegurl',
'application/vnd.aristanetworks.swi' =>
array (
0 => 'swi',
),
'swi' => 'application/vnd.aristanetworks.swi',
'application/vnd.astraea-software.iota' =>
array (
0 => 'iota',
),
'iota' => 'application/vnd.astraea-software.iota',
'application/vnd.audiograph' =>
array (
0 => 'aep',
),
'aep' => 'application/vnd.audiograph',
'application/vnd.blueice.multipass' =>
array (
0 => 'mpm',
),
'mpm' => 'application/vnd.blueice.multipass',
'application/vnd.bmi' =>
array (
0 => 'bmi',
),
'bmi' => 'application/vnd.bmi',
'application/vnd.businessobjects' =>
array (
0 => 'rep',
),
'rep' => 'application/vnd.businessobjects',
'application/vnd.chemdraw+xml' =>
array (
0 => 'cdxml',
),
'cdxml' => 'application/vnd.chemdraw+xml',
'application/vnd.chipnuts.karaoke-mmd' =>
array (
0 => 'mmd',
),
'mmd' => 'application/vnd.chipnuts.karaoke-mmd',
'application/vnd.cinderella' =>
array (
0 => 'cdy',
),
'cdy' => 'application/vnd.cinderella',
'application/vnd.claymore' =>
array (
0 => 'cla',
),
'cla' => 'application/vnd.claymore',
'application/vnd.cloanto.rp9' =>
array (
0 => 'rp9',
),
'rp9' => 'application/vnd.cloanto.rp9',
'application/vnd.clonk.c4group' =>
array (
0 => 'c4g',
1 => 'c4d',
2 => 'c4f',
3 => 'c4p',
4 => 'c4u',
),
'c4g' => 'application/vnd.clonk.c4group',
'c4d' => 'application/vnd.clonk.c4group',
'c4f' => 'application/vnd.clonk.c4group',
'c4p' => 'application/vnd.clonk.c4group',
'c4u' => 'application/vnd.clonk.c4group',
'application/vnd.cluetrust.cartomobile-config' =>
array (
0 => 'c11amc',
),
'c11amc' => 'application/vnd.cluetrust.cartomobile-config',
'application/vnd.cluetrust.cartomobile-config-pkg' =>
array (
0 => 'c11amz',
),
'c11amz' => 'application/vnd.cluetrust.cartomobile-config-pkg',
'application/vnd.commonspace' =>
array (
0 => 'csp',
),
'csp' => 'application/vnd.commonspace',
'application/vnd.contact.cmsg' =>
array (
0 => 'cdbcmsg',
),
'cdbcmsg' => 'application/vnd.contact.cmsg',
'application/vnd.cosmocaller' =>
array (
0 => 'cmc',
),
'cmc' => 'application/vnd.cosmocaller',
'application/vnd.crick.clicker' =>
array (
0 => 'clkx',
),
'clkx' => 'application/vnd.crick.clicker',
'application/vnd.crick.clicker.keyboard' =>
array (
0 => 'clkk',
),
'clkk' => 'application/vnd.crick.clicker.keyboard',
'application/vnd.crick.clicker.palette' =>
array (
0 => 'clkp',
),
'clkp' => 'application/vnd.crick.clicker.palette',
'application/vnd.crick.clicker.template' =>
array (
0 => 'clkt',
),
'clkt' => 'application/vnd.crick.clicker.template',
'application/vnd.crick.clicker.wordbank' =>
array (
0 => 'clkw',
),
'clkw' => 'application/vnd.crick.clicker.wordbank',
'application/vnd.criticaltools.wbs+xml' =>
array (
0 => 'wbs',
),
'wbs' => 'application/vnd.criticaltools.wbs+xml',
'application/vnd.ctc-posml' =>
array (
0 => 'pml',
),
'pml' => 'application/vnd.ctc-posml',
'application/vnd.cups-ppd' =>
array (
0 => 'ppd',
),
'ppd' => 'application/vnd.cups-ppd',
'application/vnd.curl.car' =>
array (
0 => 'car',
),
'car' => 'application/vnd.curl.car',
'application/vnd.curl.pcurl' =>
array (
0 => 'pcurl',
),
'pcurl' => 'application/vnd.curl.pcurl',
'application/vnd.dart' =>
array (
0 => 'dart',
),
'dart' => 'application/vnd.dart',
'application/vnd.data-vision.rdz' =>
array (
0 => 'rdz',
),
'rdz' => 'application/vnd.data-vision.rdz',
'application/vnd.dece.data' =>
array (
0 => 'uvf',
1 => 'uvvf',
2 => 'uvd',
3 => 'uvvd',
),
'uvf' => 'application/vnd.dece.data',
'uvvf' => 'application/vnd.dece.data',
'uvd' => 'application/vnd.dece.data',
'uvvd' => 'application/vnd.dece.data',
'application/vnd.dece.ttml+xml' =>
array (
0 => 'uvt',
1 => 'uvvt',
),
'uvt' => 'application/vnd.dece.ttml+xml',
'uvvt' => 'application/vnd.dece.ttml+xml',
'application/vnd.dece.unspecified' =>
array (
0 => 'uvx',
1 => 'uvvx',
),
'uvx' => 'application/vnd.dece.unspecified',
'uvvx' => 'application/vnd.dece.unspecified',
'application/vnd.dece.zip' =>
array (
0 => 'uvz',
1 => 'uvvz',
),
'uvz' => 'application/vnd.dece.zip',
'uvvz' => 'application/vnd.dece.zip',
'application/vnd.denovo.fcselayout-link' =>
array (
0 => 'fe_launch',
),
'fe_launch' => 'application/vnd.denovo.fcselayout-link',
'application/vnd.dna' =>
array (
0 => 'dna',
),
'dna' => 'application/vnd.dna',
'application/vnd.dolby.mlp' =>
array (
0 => 'mlp',
),
'mlp' => 'application/vnd.dolby.mlp',
'application/vnd.dpgraph' =>
array (
0 => 'dpg',
),
'dpg' => 'application/vnd.dpgraph',
'application/vnd.dreamfactory' =>
array (
0 => 'dfac',
),
'dfac' => 'application/vnd.dreamfactory',
'application/vnd.ds-keypoint' =>
array (
0 => 'kpxx',
),
'kpxx' => 'application/vnd.ds-keypoint',
'application/vnd.dvb.ait' =>
array (
0 => 'ait',
),
'ait' => 'application/vnd.dvb.ait',
'application/vnd.dvb.service' =>
array (
0 => 'svc',
),
'svc' => 'application/vnd.dvb.service',
'application/vnd.dynageo' =>
array (
0 => 'geo',
),
'geo' => 'application/vnd.dynageo',
'application/vnd.ecowin.chart' =>
array (
0 => 'mag',
),
'mag' => 'application/vnd.ecowin.chart',
'application/vnd.enliven' =>
array (
0 => 'nml',
),
'nml' => 'application/vnd.enliven',
'application/vnd.epson.esf' =>
array (
0 => 'esf',
),
'esf' => 'application/vnd.epson.esf',
'application/vnd.epson.msf' =>
array (
0 => 'msf',
),
'msf' => 'application/vnd.epson.msf',
'application/vnd.epson.quickanime' =>
array (
0 => 'qam',
),
'qam' => 'application/vnd.epson.quickanime',
'application/vnd.epson.salt' =>
array (
0 => 'slt',
),
'slt' => 'application/vnd.epson.salt',
'application/vnd.epson.ssf' =>
array (
0 => 'ssf',
),
'ssf' => 'application/vnd.epson.ssf',
'application/vnd.eszigno3+xml' =>
array (
0 => 'es3',
1 => 'et3',
),
'es3' => 'application/vnd.eszigno3+xml',
'et3' => 'application/vnd.eszigno3+xml',
'application/vnd.ezpix-album' =>
array (
0 => 'ez2',
),
'ez2' => 'application/vnd.ezpix-album',
'application/vnd.ezpix-package' =>
array (
0 => 'ez3',
),
'ez3' => 'application/vnd.ezpix-package',
'application/vnd.fdf' =>
array (
0 => 'fdf',
),
'fdf' => 'application/vnd.fdf',
'application/vnd.fdsn.mseed' =>
array (
0 => 'mseed',
),
'mseed' => 'application/vnd.fdsn.mseed',
'application/vnd.fdsn.seed' =>
array (
0 => 'seed',
1 => 'dataless',
),
'seed' => 'application/vnd.fdsn.seed',
'dataless' => 'application/vnd.fdsn.seed',
'application/vnd.flographit' =>
array (
0 => 'gph',
),
'gph' => 'application/vnd.flographit',
'application/vnd.fluxtime.clip' =>
array (
0 => 'ftc',
),
'ftc' => 'application/vnd.fluxtime.clip',
'application/vnd.framemaker' =>
array (
0 => 'fm',
1 => 'frame',
2 => 'maker',
3 => 'book',
),
'fm' => 'application/vnd.framemaker',
'frame' => 'application/vnd.framemaker',
'maker' => 'application/vnd.framemaker',
'book' => 'application/vnd.framemaker',
'application/vnd.frogans.fnc' =>
array (
0 => 'fnc',
),
'fnc' => 'application/vnd.frogans.fnc',
'application/vnd.frogans.ltf' =>
array (
0 => 'ltf',
),
'ltf' => 'application/vnd.frogans.ltf',
'application/vnd.fsc.weblaunch' =>
array (
0 => 'fsc',
),
'fsc' => 'application/vnd.fsc.weblaunch',
'application/vnd.fujitsu.oasys' =>
array (
0 => 'oas',
),
'oas' => 'application/vnd.fujitsu.oasys',
'application/vnd.fujitsu.oasys2' =>
array (
0 => 'oa2',
),
'oa2' => 'application/vnd.fujitsu.oasys2',
'application/vnd.fujitsu.oasys3' =>
array (
0 => 'oa3',
),
'oa3' => 'application/vnd.fujitsu.oasys3',
'application/vnd.fujitsu.oasysgp' =>
array (
0 => 'fg5',
),
'fg5' => 'application/vnd.fujitsu.oasysgp',
'application/vnd.fujitsu.oasysprs' =>
array (
0 => 'bh2',
),
'bh2' => 'application/vnd.fujitsu.oasysprs',
'application/vnd.fujixerox.ddd' =>
array (
0 => 'ddd',
),
'ddd' => 'application/vnd.fujixerox.ddd',
'application/vnd.fujixerox.docuworks' =>
array (
0 => 'xdw',
),
'xdw' => 'application/vnd.fujixerox.docuworks',
'application/vnd.fujixerox.docuworks.binder' =>
array (
0 => 'xbd',
),
'xbd' => 'application/vnd.fujixerox.docuworks.binder',
'application/vnd.fuzzysheet' =>
array (
0 => 'fzs',
),
'fzs' => 'application/vnd.fuzzysheet',
'application/vnd.genomatix.tuxedo' =>
array (
0 => 'txd',
),
'txd' => 'application/vnd.genomatix.tuxedo',
'application/vnd.geogebra.file' =>
array (
0 => 'ggb',
),
'ggb' => 'application/vnd.geogebra.file',
'application/vnd.geogebra.slides' =>
array (
0 => 'ggs',
),
'ggs' => 'application/vnd.geogebra.slides',
'application/vnd.geogebra.tool' =>
array (
0 => 'ggt',
),
'ggt' => 'application/vnd.geogebra.tool',
'application/vnd.geometry-explorer' =>
array (
0 => 'gex',
1 => 'gre',
),
'gex' => 'application/vnd.geometry-explorer',
'gre' => 'application/vnd.geometry-explorer',
'application/vnd.geonext' =>
array (
0 => 'gxt',
),
'gxt' => 'application/vnd.geonext',
'application/vnd.geoplan' =>
array (
0 => 'g2w',
),
'g2w' => 'application/vnd.geoplan',
'application/vnd.geospace' =>
array (
0 => 'g3w',
),
'g3w' => 'application/vnd.geospace',
'application/vnd.gmx' =>
array (
0 => 'gmx',
),
'gmx' => 'application/vnd.gmx',
'application/vnd.google-earth.kml+xml' =>
array (
0 => 'kml',
),
'kml' => 'application/vnd.google-earth.kml+xml',
'application/vnd.google-earth.kmz' =>
array (
0 => 'kmz',
),
'kmz' => 'application/vnd.google-earth.kmz',
'application/vnd.grafeq' =>
array (
0 => 'gqf',
1 => 'gqs',
),
'gqf' => 'application/vnd.grafeq',
'gqs' => 'application/vnd.grafeq',
'application/vnd.groove-account' =>
array (
0 => 'gac',
),
'gac' => 'application/vnd.groove-account',
'application/vnd.groove-help' =>
array (
0 => 'ghf',
),
'ghf' => 'application/vnd.groove-help',
'application/vnd.groove-identity-message' =>
array (
0 => 'gim',
),
'gim' => 'application/vnd.groove-identity-message',
'application/vnd.groove-injector' =>
array (
0 => 'grv',
),
'grv' => 'application/vnd.groove-injector',
'application/vnd.groove-tool-message' =>
array (
0 => 'gtm',
),
'gtm' => 'application/vnd.groove-tool-message',
'application/vnd.groove-tool-template' =>
array (
0 => 'tpl',
),
'tpl' => 'application/vnd.groove-tool-template',
'application/vnd.groove-vcard' =>
array (
0 => 'vcg',
),
'vcg' => 'application/vnd.groove-vcard',
'application/vnd.hal+xml' =>
array (
0 => 'hal',
),
'hal' => 'application/vnd.hal+xml',
'application/vnd.handheld-entertainment+xml' =>
array (
0 => 'zmm',
),
'zmm' => 'application/vnd.handheld-entertainment+xml',
'application/vnd.hbci' =>
array (
0 => 'hbci',
),
'hbci' => 'application/vnd.hbci',
'application/vnd.hhe.lesson-player' =>
array (
0 => 'les',
),
'les' => 'application/vnd.hhe.lesson-player',
'application/vnd.hp-hpgl' =>
array (
0 => 'hpgl',
),
'hpgl' => 'application/vnd.hp-hpgl',
'application/vnd.hp-hpid' =>
array (
0 => 'hpid',
),
'hpid' => 'application/vnd.hp-hpid',
'application/vnd.hp-hps' =>
array (
0 => 'hps',
),
'hps' => 'application/vnd.hp-hps',
'application/vnd.hp-jlyt' =>
array (
0 => 'jlt',
),
'jlt' => 'application/vnd.hp-jlyt',
'application/vnd.hp-pcl' =>
array (
0 => 'pcl',
),
'pcl' => 'application/vnd.hp-pcl',
'application/vnd.hp-pclxl' =>
array (
0 => 'pclxl',
),
'pclxl' => 'application/vnd.hp-pclxl',
'application/vnd.hydrostatix.sof-data' =>
array (
0 => 'sfd-hdstx',
),
'sfd-hdstx' => 'application/vnd.hydrostatix.sof-data',
'application/vnd.ibm.minipay' =>
array (
0 => 'mpy',
),
'mpy' => 'application/vnd.ibm.minipay',
'application/vnd.ibm.modcap' =>
array (
0 => 'afp',
1 => 'listafp',
2 => 'list3820',
),
'afp' => 'application/vnd.ibm.modcap',
'listafp' => 'application/vnd.ibm.modcap',
'list3820' => 'application/vnd.ibm.modcap',
'application/vnd.ibm.rights-management' =>
array (
0 => 'irm',
),
'irm' => 'application/vnd.ibm.rights-management',
'application/vnd.ibm.secure-container' =>
array (
0 => 'sc',
),
'sc' => 'application/vnd.ibm.secure-container',
'application/vnd.iccprofile' =>
array (
0 => 'icc',
1 => 'icm',
),
'icc' => 'application/vnd.iccprofile',
'icm' => 'application/vnd.iccprofile',
'application/vnd.igloader' =>
array (
0 => 'igl',
),
'igl' => 'application/vnd.igloader',
'application/vnd.immervision-ivp' =>
array (
0 => 'ivp',
),
'ivp' => 'application/vnd.immervision-ivp',
'application/vnd.immervision-ivu' =>
array (
0 => 'ivu',
),
'ivu' => 'application/vnd.immervision-ivu',
'application/vnd.insors.igm' =>
array (
0 => 'igm',
),
'igm' => 'application/vnd.insors.igm',
'application/vnd.intercon.formnet' =>
array (
0 => 'xpw',
1 => 'xpx',
),
'xpw' => 'application/vnd.intercon.formnet',
'xpx' => 'application/vnd.intercon.formnet',
'application/vnd.intergeo' =>
array (
0 => 'i2g',
),
'i2g' => 'application/vnd.intergeo',
'application/vnd.intu.qbo' =>
array (
0 => 'qbo',
),
'qbo' => 'application/vnd.intu.qbo',
'application/vnd.intu.qfx' =>
array (
0 => 'qfx',
),
'qfx' => 'application/vnd.intu.qfx',
'application/vnd.ipunplugged.rcprofile' =>
array (
0 => 'rcprofile',
),
'rcprofile' => 'application/vnd.ipunplugged.rcprofile',
'application/vnd.irepository.package+xml' =>
array (
0 => 'irp',
),
'irp' => 'application/vnd.irepository.package+xml',
'application/vnd.is-xpr' =>
array (
0 => 'xpr',
),
'xpr' => 'application/vnd.is-xpr',
'application/vnd.isac.fcs' =>
array (
0 => 'fcs',
),
'fcs' => 'application/vnd.isac.fcs',
'application/vnd.jam' =>
array (
0 => 'jam',
),
'jam' => 'application/vnd.jam',
'application/vnd.jcp.javame.midlet-rms' =>
array (
0 => 'rms',
),
'rms' => 'application/vnd.jcp.javame.midlet-rms',
'application/vnd.jisp' =>
array (
0 => 'jisp',
),
'jisp' => 'application/vnd.jisp',
'application/vnd.joost.joda-archive' =>
array (
0 => 'joda',
),
'joda' => 'application/vnd.joost.joda-archive',
'application/vnd.kahootz' =>
array (
0 => 'ktz',
1 => 'ktr',
),
'ktz' => 'application/vnd.kahootz',
'ktr' => 'application/vnd.kahootz',
'application/vnd.kde.karbon' =>
array (
0 => 'karbon',
),
'karbon' => 'application/vnd.kde.karbon',
'application/vnd.kde.kchart' =>
array (
0 => 'chrt',
),
'chrt' => 'application/vnd.kde.kchart',
'application/vnd.kde.kformula' =>
array (
0 => 'kfo',
),
'kfo' => 'application/vnd.kde.kformula',
'application/vnd.kde.kivio' =>
array (
0 => 'flw',
),
'flw' => 'application/vnd.kde.kivio',
'application/vnd.kde.kontour' =>
array (
0 => 'kon',
),
'kon' => 'application/vnd.kde.kontour',
'application/vnd.kde.kpresenter' =>
array (
0 => 'kpr',
1 => 'kpt',
),
'kpr' => 'application/vnd.kde.kpresenter',
'kpt' => 'application/vnd.kde.kpresenter',
'application/vnd.kde.kspread' =>
array (
0 => 'ksp',
),
'ksp' => 'application/vnd.kde.kspread',
'application/vnd.kde.kword' =>
array (
0 => 'kwd',
1 => 'kwt',
),
'kwd' => 'application/vnd.kde.kword',
'kwt' => 'application/vnd.kde.kword',
'application/vnd.kenameaapp' =>
array (
0 => 'htke',
),
'htke' => 'application/vnd.kenameaapp',
'application/vnd.kidspiration' =>
array (
0 => 'kia',
),
'kia' => 'application/vnd.kidspiration',
'application/vnd.kinar' =>
array (
0 => 'kne',
1 => 'knp',
),
'kne' => 'application/vnd.kinar',
'knp' => 'application/vnd.kinar',
'application/vnd.koan' =>
array (
0 => 'skp',
1 => 'skd',
2 => 'skt',
3 => 'skm',
),
'skp' => 'application/vnd.koan',
'skd' => 'application/vnd.koan',
'skt' => 'application/vnd.koan',
'skm' => 'application/vnd.koan',
'application/vnd.kodak-descriptor' =>
array (
0 => 'sse',
),
'sse' => 'application/vnd.kodak-descriptor',
'application/vnd.las.las+xml' =>
array (
0 => 'lasxml',
),
'lasxml' => 'application/vnd.las.las+xml',
'application/vnd.llamagraphics.life-balance.desktop' =>
array (
0 => 'lbd',
),
'lbd' => 'application/vnd.llamagraphics.life-balance.desktop',
'application/vnd.llamagraphics.life-balance.exchange+xml' =>
array (
0 => 'lbe',
),
'lbe' => 'application/vnd.llamagraphics.life-balance.exchange+xml',
'application/vnd.lotus-1-2-3' =>
array (
0 => '123',
),
123 => 'application/vnd.lotus-1-2-3',
'application/vnd.lotus-approach' =>
array (
0 => 'apr',
),
'apr' => 'application/vnd.lotus-approach',
'application/vnd.lotus-freelance' =>
array (
0 => 'pre',
),
'pre' => 'application/vnd.lotus-freelance',
'application/vnd.lotus-notes' =>
array (
0 => 'nsf',
),
'nsf' => 'application/vnd.lotus-notes',
'application/vnd.lotus-organizer' =>
array (
0 => 'org',
),
'org' => 'application/vnd.lotus-organizer',
'application/vnd.lotus-screencam' =>
array (
0 => 'scm',
),
'scm' => 'application/vnd.lotus-screencam',
'application/vnd.lotus-wordpro' =>
array (
0 => 'lwp',
),
'lwp' => 'application/vnd.lotus-wordpro',
'application/vnd.macports.portpkg' =>
array (
0 => 'portpkg',
),
'portpkg' => 'application/vnd.macports.portpkg',
'application/vnd.mcd' =>
array (
0 => 'mcd',
),
'mcd' => 'application/vnd.mcd',
'application/vnd.medcalcdata' =>
array (
0 => 'mc1',
),
'mc1' => 'application/vnd.medcalcdata',
'application/vnd.mediastation.cdkey' =>
array (
0 => 'cdkey',
),
'cdkey' => 'application/vnd.mediastation.cdkey',
'application/vnd.mfer' =>
array (
0 => 'mwf',
),
'mwf' => 'application/vnd.mfer',
'application/vnd.mfmp' =>
array (
0 => 'mfm',
),
'mfm' => 'application/vnd.mfmp',
'application/vnd.micrografx.flo' =>
array (
0 => 'flo',
),
'flo' => 'application/vnd.micrografx.flo',
'application/vnd.micrografx.igx' =>
array (
0 => 'igx',
),
'igx' => 'application/vnd.micrografx.igx',
'application/vnd.mif' =>
array (
0 => 'mif',
),
'mif' => 'application/vnd.mif',
'application/vnd.mobius.daf' =>
array (
0 => 'daf',
),
'daf' => 'application/vnd.mobius.daf',
'application/vnd.mobius.dis' =>
array (
0 => 'dis',
),
'dis' => 'application/vnd.mobius.dis',
'application/vnd.mobius.mbk' =>
array (
0 => 'mbk',
),
'mbk' => 'application/vnd.mobius.mbk',
'application/vnd.mobius.mqy' =>
array (
0 => 'mqy',
),
'mqy' => 'application/vnd.mobius.mqy',
'application/vnd.mobius.msl' =>
array (
0 => 'msl',
),
'msl' => 'application/vnd.mobius.msl',
'application/vnd.mobius.plc' =>
array (
0 => 'plc',
),
'plc' => 'application/vnd.mobius.plc',
'application/vnd.mobius.txf' =>
array (
0 => 'txf',
),
'txf' => 'application/vnd.mobius.txf',
'application/vnd.mophun.application' =>
array (
0 => 'mpn',
),
'mpn' => 'application/vnd.mophun.application',
'application/vnd.mophun.certificate' =>
array (
0 => 'mpc',
),
'mpc' => 'application/vnd.mophun.certificate',
'application/vnd.mozilla.xul+xml' =>
array (
0 => 'xul',
),
'xul' => 'application/vnd.mozilla.xul+xml',
'application/vnd.ms-artgalry' =>
array (
0 => 'cil',
),
'cil' => 'application/vnd.ms-artgalry',
'application/vnd.ms-cab-compressed' =>
array (
0 => 'cab',
),
'cab' => 'application/vnd.ms-cab-compressed',
'application/vnd.ms-excel' =>
array (
0 => 'xls',
1 => 'xlm',
2 => 'xla',
3 => 'xlc',
4 => 'xlt',
5 => 'xlw',
),
'xls' => 'application/vnd.ms-excel',
'xlm' => 'application/vnd.ms-excel',
'xla' => 'application/vnd.ms-excel',
'xlc' => 'application/vnd.ms-excel',
'xlt' => 'application/vnd.ms-excel',
'xlw' => 'application/vnd.ms-excel',
'application/vnd.ms-excel.addin.macroenabled.12' =>
array (
0 => 'xlam',
),
'xlam' => 'application/vnd.ms-excel.addin.macroenabled.12',
'application/vnd.ms-excel.sheet.binary.macroenabled.12' =>
array (
0 => 'xlsb',
),
'xlsb' => 'application/vnd.ms-excel.sheet.binary.macroenabled.12',
'application/vnd.ms-excel.sheet.macroenabled.12' =>
array (
0 => 'xlsm',
),
'xlsm' => 'application/vnd.ms-excel.sheet.macroenabled.12',
'application/vnd.ms-excel.template.macroenabled.12' =>
array (
0 => 'xltm',
),
'xltm' => 'application/vnd.ms-excel.template.macroenabled.12',
'application/vnd.ms-fontobject' =>
array (
0 => 'eot',
),
'eot' => 'application/vnd.ms-fontobject',
'application/vnd.ms-htmlhelp' =>
array (
0 => 'chm',
),
'chm' => 'application/vnd.ms-htmlhelp',
'application/vnd.ms-ims' =>
array (
0 => 'ims',
),
'ims' => 'application/vnd.ms-ims',
'application/vnd.ms-lrm' =>
array (
0 => 'lrm',
),
'lrm' => 'application/vnd.ms-lrm',
'application/vnd.ms-officetheme' =>
array (
0 => 'thmx',
),
'thmx' => 'application/vnd.ms-officetheme',
'application/vnd.ms-pki.seccat' =>
array (
0 => 'cat',
),
'cat' => 'application/vnd.ms-pki.seccat',
'application/vnd.ms-pki.stl' =>
array (
0 => 'stl',
),
'stl' => 'application/vnd.ms-pki.stl',
'application/vnd.ms-powerpoint' =>
array (
0 => 'ppt',
1 => 'pps',
2 => 'pot',
),
'ppt' => 'application/vnd.ms-powerpoint',
'pps' => 'application/vnd.ms-powerpoint',
'pot' => 'application/vnd.ms-powerpoint',
'application/vnd.ms-powerpoint.addin.macroenabled.12' =>
array (
0 => 'ppam',
),
'ppam' => 'application/vnd.ms-powerpoint.addin.macroenabled.12',
'application/vnd.ms-powerpoint.presentation.macroenabled.12' =>
array (
0 => 'pptm',
),
'pptm' => 'application/vnd.ms-powerpoint.presentation.macroenabled.12',
'application/vnd.ms-powerpoint.slide.macroenabled.12' =>
array (
0 => 'sldm',
),
'sldm' => 'application/vnd.ms-powerpoint.slide.macroenabled.12',
'application/vnd.ms-powerpoint.slideshow.macroenabled.12' =>
array (
0 => 'ppsm',
),
'ppsm' => 'application/vnd.ms-powerpoint.slideshow.macroenabled.12',
'application/vnd.ms-powerpoint.template.macroenabled.12' =>
array (
0 => 'potm',
),
'potm' => 'application/vnd.ms-powerpoint.template.macroenabled.12',
'application/vnd.ms-project' =>
array (
0 => 'mpp',
1 => 'mpt',
),
'mpp' => 'application/vnd.ms-project',
'mpt' => 'application/vnd.ms-project',
'application/vnd.ms-word.document.macroenabled.12' =>
array (
0 => 'docm',
),
'docm' => 'application/vnd.ms-word.document.macroenabled.12',
'application/vnd.ms-word.template.macroenabled.12' =>
array (
0 => 'dotm',
),
'dotm' => 'application/vnd.ms-word.template.macroenabled.12',
'application/vnd.ms-works' =>
array (
0 => 'wps',
1 => 'wks',
2 => 'wcm',
3 => 'wdb',
),
'wps' => 'application/vnd.ms-works',
'wks' => 'application/vnd.ms-works',
'wcm' => 'application/vnd.ms-works',
'wdb' => 'application/vnd.ms-works',
'application/vnd.ms-wpl' =>
array (
0 => 'wpl',
),
'wpl' => 'application/vnd.ms-wpl',
'application/vnd.ms-xpsdocument' =>
array (
0 => 'xps',
),
'xps' => 'application/vnd.ms-xpsdocument',
'application/vnd.mseq' =>
array (
0 => 'mseq',
),
'mseq' => 'application/vnd.mseq',
'application/vnd.musician' =>
array (
0 => 'mus',
),
'mus' => 'application/vnd.musician',
'application/vnd.muvee.style' =>
array (
0 => 'msty',
),
'msty' => 'application/vnd.muvee.style',
'application/vnd.mynfc' =>
array (
0 => 'taglet',
),
'taglet' => 'application/vnd.mynfc',
'application/vnd.neurolanguage.nlu' =>
array (
0 => 'nlu',
),
'nlu' => 'application/vnd.neurolanguage.nlu',
'application/vnd.nitf' =>
array (
0 => 'ntf',
1 => 'nitf',
),
'ntf' => 'application/vnd.nitf',
'nitf' => 'application/vnd.nitf',
'application/vnd.noblenet-directory' =>
array (
0 => 'nnd',
),
'nnd' => 'application/vnd.noblenet-directory',
'application/vnd.noblenet-sealer' =>
array (
0 => 'nns',
),
'nns' => 'application/vnd.noblenet-sealer',
'application/vnd.noblenet-web' =>
array (
0 => 'nnw',
),
'nnw' => 'application/vnd.noblenet-web',
'application/vnd.nokia.n-gage.data' =>
array (
0 => 'ngdat',
),
'ngdat' => 'application/vnd.nokia.n-gage.data',
'application/vnd.nokia.n-gage.symbian.install' =>
array (
0 => 'n-gage',
),
'n-gage' => 'application/vnd.nokia.n-gage.symbian.install',
'application/vnd.nokia.radio-preset' =>
array (
0 => 'rpst',
),
'rpst' => 'application/vnd.nokia.radio-preset',
'application/vnd.nokia.radio-presets' =>
array (
0 => 'rpss',
),
'rpss' => 'application/vnd.nokia.radio-presets',
'application/vnd.novadigm.edm' =>
array (
0 => 'edm',
),
'edm' => 'application/vnd.novadigm.edm',
'application/vnd.novadigm.edx' =>
array (
0 => 'edx',
),
'edx' => 'application/vnd.novadigm.edx',
'application/vnd.novadigm.ext' =>
array (
0 => 'ext',
),
'ext' => 'application/vnd.novadigm.ext',
'application/vnd.oasis.opendocument.chart' =>
array (
0 => 'odc',
),
'odc' => 'application/vnd.oasis.opendocument.chart',
'application/vnd.oasis.opendocument.chart-template' =>
array (
0 => 'otc',
),
'otc' => 'application/vnd.oasis.opendocument.chart-template',
'application/vnd.oasis.opendocument.database' =>
array (
0 => 'odb',
),
'odb' => 'application/vnd.oasis.opendocument.database',
'application/vnd.oasis.opendocument.formula' =>
array (
0 => 'odf',
),
'odf' => 'application/vnd.oasis.opendocument.formula',
'application/vnd.oasis.opendocument.formula-template' =>
array (
0 => 'odft',
),
'odft' => 'application/vnd.oasis.opendocument.formula-template',
'application/vnd.oasis.opendocument.graphics' =>
array (
0 => 'odg',
),
'odg' => 'application/vnd.oasis.opendocument.graphics',
'application/vnd.oasis.opendocument.graphics-template' =>
array (
0 => 'otg',
),
'otg' => 'application/vnd.oasis.opendocument.graphics-template',
'application/vnd.oasis.opendocument.image' =>
array (
0 => 'odi',
),
'odi' => 'application/vnd.oasis.opendocument.image',
'application/vnd.oasis.opendocument.image-template' =>
array (
0 => 'oti',
),
'oti' => 'application/vnd.oasis.opendocument.image-template',
'application/vnd.oasis.opendocument.presentation' =>
array (
0 => 'odp',
),
'odp' => 'application/vnd.oasis.opendocument.presentation',
'application/vnd.oasis.opendocument.presentation-template' =>
array (
0 => 'otp',
),
'otp' => 'application/vnd.oasis.opendocument.presentation-template',
'application/vnd.oasis.opendocument.spreadsheet' =>
array (
0 => 'ods',
),
'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
'application/vnd.oasis.opendocument.spreadsheet-template' =>
array (
0 => 'ots',
),
'ots' => 'application/vnd.oasis.opendocument.spreadsheet-template',
'application/vnd.oasis.opendocument.text' =>
array (
0 => 'odt',
),
'odt' => 'application/vnd.oasis.opendocument.text',
'application/vnd.oasis.opendocument.text-master' =>
array (
0 => 'odm',
),
'odm' => 'application/vnd.oasis.opendocument.text-master',
'application/vnd.oasis.opendocument.text-template' =>
array (
0 => 'ott',
),
'ott' => 'application/vnd.oasis.opendocument.text-template',
'application/vnd.oasis.opendocument.text-web' =>
array (
0 => 'oth',
),
'oth' => 'application/vnd.oasis.opendocument.text-web',
'application/vnd.olpc-sugar' =>
array (
0 => 'xo',
),
'xo' => 'application/vnd.olpc-sugar',
'application/vnd.oma.dd2+xml' =>
array (
0 => 'dd2',
),
'dd2' => 'application/vnd.oma.dd2+xml',
'application/vnd.openofficeorg.extension' =>
array (
0 => 'oxt',
),
'oxt' => 'application/vnd.openofficeorg.extension',
'application/vnd.openxmlformats-officedocument.presentationml.presentation' =>
array (
0 => 'pptx',
),
'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
'application/vnd.openxmlformats-officedocument.presentationml.slide' =>
array (
0 => 'sldx',
),
'sldx' => 'application/vnd.openxmlformats-officedocument.presentationml.slide',
'application/vnd.openxmlformats-officedocument.presentationml.slideshow' =>
array (
0 => 'ppsx',
),
'ppsx' => 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
'application/vnd.openxmlformats-officedocument.presentationml.template' =>
array (
0 => 'potx',
),
'potx' => 'application/vnd.openxmlformats-officedocument.presentationml.template',
'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' =>
array (
0 => 'xlsx',
),
'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
'application/vnd.openxmlformats-officedocument.spreadsheetml.template' =>
array (
0 => 'xltx',
),
'xltx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
'application/vnd.openxmlformats-officedocument.wordprocessingml.document' =>
array (
0 => 'docx',
),
'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
'application/vnd.openxmlformats-officedocument.wordprocessingml.template' =>
array (
0 => 'dotx',
),
'dotx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
'application/vnd.osgeo.mapguide.package' =>
array (
0 => 'mgp',
),
'mgp' => 'application/vnd.osgeo.mapguide.package',
'application/vnd.osgi.dp' =>
array (
0 => 'dp',
),
'dp' => 'application/vnd.osgi.dp',
'application/vnd.osgi.subsystem' =>
array (
0 => 'esa',
),
'esa' => 'application/vnd.osgi.subsystem',
'application/vnd.palm' =>
array (
0 => 'pdb',
1 => 'pqa',
2 => 'oprc',
),
'pdb' => 'application/vnd.palm',
'pqa' => 'application/vnd.palm',
'oprc' => 'application/vnd.palm',
'application/vnd.pawaafile' =>
array (
0 => 'paw',
),
'paw' => 'application/vnd.pawaafile',
'application/vnd.pg.format' =>
array (
0 => 'str',
),
'str' => 'application/vnd.pg.format',
'application/vnd.pg.osasli' =>
array (
0 => 'ei6',
),
'ei6' => 'application/vnd.pg.osasli',
'application/vnd.picsel' =>
array (
0 => 'efif',
),
'efif' => 'application/vnd.picsel',
'application/vnd.pmi.widget' =>
array (
0 => 'wg',
),
'wg' => 'application/vnd.pmi.widget',
'application/vnd.pocketlearn' =>
array (
0 => 'plf',
),
'plf' => 'application/vnd.pocketlearn',
'application/vnd.powerbuilder6' =>
array (
0 => 'pbd',
),
'pbd' => 'application/vnd.powerbuilder6',
'application/vnd.previewsystems.box' =>
array (
0 => 'box',
),
'box' => 'application/vnd.previewsystems.box',
'application/vnd.proteus.magazine' =>
array (
0 => 'mgz',
),
'mgz' => 'application/vnd.proteus.magazine',
'application/vnd.publishare-delta-tree' =>
array (
0 => 'qps',
),
'qps' => 'application/vnd.publishare-delta-tree',
'application/vnd.pvi.ptid1' =>
array (
0 => 'ptid',
),
'ptid' => 'application/vnd.pvi.ptid1',
'application/vnd.quark.quarkxpress' =>
array (
0 => 'qxd',
1 => 'qxt',
2 => 'qwd',
3 => 'qwt',
4 => 'qxl',
5 => 'qxb',
),
'qxd' => 'application/vnd.quark.quarkxpress',
'qxt' => 'application/vnd.quark.quarkxpress',
'qwd' => 'application/vnd.quark.quarkxpress',
'qwt' => 'application/vnd.quark.quarkxpress',
'qxl' => 'application/vnd.quark.quarkxpress',
'qxb' => 'application/vnd.quark.quarkxpress',
'application/vnd.realvnc.bed' =>
array (
0 => 'bed',
),
'bed' => 'application/vnd.realvnc.bed',
'application/vnd.recordare.musicxml' =>
array (
0 => 'mxl',
),
'mxl' => 'application/vnd.recordare.musicxml',
'application/vnd.recordare.musicxml+xml' =>
array (
0 => 'musicxml',
),
'musicxml' => 'application/vnd.recordare.musicxml+xml',
'application/vnd.rig.cryptonote' =>
array (
0 => 'cryptonote',
),
'cryptonote' => 'application/vnd.rig.cryptonote',
'application/vnd.rim.cod' =>
array (
0 => 'cod',
),
'cod' => 'application/vnd.rim.cod',
'application/vnd.rn-realmedia' =>
array (
0 => 'rm',
),
'rm' => 'application/vnd.rn-realmedia',
'application/vnd.rn-realmedia-vbr' =>
array (
0 => 'rmvb',
),
'rmvb' => 'application/vnd.rn-realmedia-vbr',
'application/vnd.route66.link66+xml' =>
array (
0 => 'link66',
),
'link66' => 'application/vnd.route66.link66+xml',
'application/vnd.sailingtracker.track' =>
array (
0 => 'st',
),
'st' => 'application/vnd.sailingtracker.track',
'application/vnd.seemail' =>
array (
0 => 'see',
),
'see' => 'application/vnd.seemail',
'application/vnd.sema' =>
array (
0 => 'sema',
),
'sema' => 'application/vnd.sema',
'application/vnd.semd' =>
array (
0 => 'semd',
),
'semd' => 'application/vnd.semd',
'application/vnd.semf' =>
array (
0 => 'semf',
),
'semf' => 'application/vnd.semf',
'application/vnd.shana.informed.formdata' =>
array (
0 => 'ifm',
),
'ifm' => 'application/vnd.shana.informed.formdata',
'application/vnd.shana.informed.formtemplate' =>
array (
0 => 'itp',
),
'itp' => 'application/vnd.shana.informed.formtemplate',
'application/vnd.shana.informed.interchange' =>
array (
0 => 'iif',
),
'iif' => 'application/vnd.shana.informed.interchange',
'application/vnd.shana.informed.package' =>
array (
0 => 'ipk',
),
'ipk' => 'application/vnd.shana.informed.package',
'application/vnd.simtech-mindmapper' =>
array (
0 => 'twd',
1 => 'twds',
),
'twd' => 'application/vnd.simtech-mindmapper',
'twds' => 'application/vnd.simtech-mindmapper',
'application/vnd.smaf' =>
array (
0 => 'mmf',
),
'mmf' => 'application/vnd.smaf',
'application/vnd.smart.teacher' =>
array (
0 => 'teacher',
),
'teacher' => 'application/vnd.smart.teacher',
'application/vnd.solent.sdkm+xml' =>
array (
0 => 'sdkm',
1 => 'sdkd',
),
'sdkm' => 'application/vnd.solent.sdkm+xml',
'sdkd' => 'application/vnd.solent.sdkm+xml',
'application/vnd.spotfire.dxp' =>
array (
0 => 'dxp',
),
'dxp' => 'application/vnd.spotfire.dxp',
'application/vnd.spotfire.sfs' =>
array (
0 => 'sfs',
),
'sfs' => 'application/vnd.spotfire.sfs',
'application/vnd.stardivision.calc' =>
array (
0 => 'sdc',
),
'sdc' => 'application/vnd.stardivision.calc',
'application/vnd.stardivision.draw' =>
array (
0 => 'sda',
),
'sda' => 'application/vnd.stardivision.draw',
'application/vnd.stardivision.impress' =>
array (
0 => 'sdd',
),
'sdd' => 'application/vnd.stardivision.impress',
'application/vnd.stardivision.math' =>
array (
0 => 'smf',
),
'smf' => 'application/vnd.stardivision.math',
'application/vnd.stardivision.writer' =>
array (
0 => 'sdw',
1 => 'vor',
),
'sdw' => 'application/vnd.stardivision.writer',
'vor' => 'application/vnd.stardivision.writer',
'application/vnd.stardivision.writer-global' =>
array (
0 => 'sgl',
),
'sgl' => 'application/vnd.stardivision.writer-global',
'application/vnd.stepmania.package' =>
array (
0 => 'smzip',
),
'smzip' => 'application/vnd.stepmania.package',
'application/vnd.stepmania.stepchart' =>
array (
0 => 'sm',
),
'sm' => 'application/vnd.stepmania.stepchart',
'application/vnd.sun.xml.calc' =>
array (
0 => 'sxc',
),
'sxc' => 'application/vnd.sun.xml.calc',
'application/vnd.sun.xml.calc.template' =>
array (
0 => 'stc',
),
'stc' => 'application/vnd.sun.xml.calc.template',
'application/vnd.sun.xml.draw' =>
array (
0 => 'sxd',
),
'sxd' => 'application/vnd.sun.xml.draw',
'application/vnd.sun.xml.draw.template' =>
array (
0 => 'std',
),
'std' => 'application/vnd.sun.xml.draw.template',
'application/vnd.sun.xml.impress' =>
array (
0 => 'sxi',
),
'sxi' => 'application/vnd.sun.xml.impress',
'application/vnd.sun.xml.impress.template' =>
array (
0 => 'sti',
),
'sti' => 'application/vnd.sun.xml.impress.template',
'application/vnd.sun.xml.math' =>
array (
0 => 'sxm',
),
'sxm' => 'application/vnd.sun.xml.math',
'application/vnd.sun.xml.writer' =>
array (
0 => 'sxw',
),
'sxw' => 'application/vnd.sun.xml.writer',
'application/vnd.sun.xml.writer.global' =>
array (
0 => 'sxg',
),
'sxg' => 'application/vnd.sun.xml.writer.global',
'application/vnd.sun.xml.writer.template' =>
array (
0 => 'stw',
),
'stw' => 'application/vnd.sun.xml.writer.template',
'application/vnd.sus-calendar' =>
array (
0 => 'sus',
1 => 'susp',
),
'sus' => 'application/vnd.sus-calendar',
'susp' => 'application/vnd.sus-calendar',
'application/vnd.svd' =>
array (
0 => 'svd',
),
'svd' => 'application/vnd.svd',
'application/vnd.symbian.install' =>
array (
0 => 'sis',
1 => 'sisx',
),
'sis' => 'application/vnd.symbian.install',
'sisx' => 'application/vnd.symbian.install',
'application/vnd.syncml+xml' =>
array (
0 => 'xsm',
),
'xsm' => 'application/vnd.syncml+xml',
'application/vnd.syncml.dm+wbxml' =>
array (
0 => 'bdm',
),
'bdm' => 'application/vnd.syncml.dm+wbxml',
'application/vnd.syncml.dm+xml' =>
array (
0 => 'xdm',
),
'xdm' => 'application/vnd.syncml.dm+xml',
'application/vnd.tao.intent-module-archive' =>
array (
0 => 'tao',
),
'tao' => 'application/vnd.tao.intent-module-archive',
'application/vnd.tcpdump.pcap' =>
array (
0 => 'pcap',
1 => 'cap',
2 => 'dmp',
),
'pcap' => 'application/vnd.tcpdump.pcap',
'cap' => 'application/vnd.tcpdump.pcap',
'dmp' => 'application/vnd.tcpdump.pcap',
'application/vnd.tmobile-livetv' =>
array (
0 => 'tmo',
),
'tmo' => 'application/vnd.tmobile-livetv',
'application/vnd.trid.tpt' =>
array (
0 => 'tpt',
),
'tpt' => 'application/vnd.trid.tpt',
'application/vnd.triscape.mxs' =>
array (
0 => 'mxs',
),
'mxs' => 'application/vnd.triscape.mxs',
'application/vnd.trueapp' =>
array (
0 => 'tra',
),
'tra' => 'application/vnd.trueapp',
'application/vnd.ufdl' =>
array (
0 => 'ufd',
1 => 'ufdl',
),
'ufd' => 'application/vnd.ufdl',
'ufdl' => 'application/vnd.ufdl',
'application/vnd.uiq.theme' =>
array (
0 => 'utz',
),
'utz' => 'application/vnd.uiq.theme',
'application/vnd.umajin' =>
array (
0 => 'umj',
),
'umj' => 'application/vnd.umajin',
'application/vnd.unity' =>
array (
0 => 'unityweb',
),
'unityweb' => 'application/vnd.unity',
'application/vnd.uoml+xml' =>
array (
0 => 'uoml',
),
'uoml' => 'application/vnd.uoml+xml',
'application/vnd.vcx' =>
array (
0 => 'vcx',
),
'vcx' => 'application/vnd.vcx',
'application/vnd.visio' =>
array (
0 => 'vsd',
1 => 'vst',
2 => 'vss',
3 => 'vsw',
),
'vsd' => 'application/vnd.visio',
'vst' => 'application/vnd.visio',
'vss' => 'application/vnd.visio',
'vsw' => 'application/vnd.visio',
'application/vnd.visionary' =>
array (
0 => 'vis',
),
'vis' => 'application/vnd.visionary',
'application/vnd.vsf' =>
array (
0 => 'vsf',
),
'vsf' => 'application/vnd.vsf',
'application/vnd.wap.wbxml' =>
array (
0 => 'wbxml',
),
'wbxml' => 'application/vnd.wap.wbxml',
'application/vnd.wap.wmlc' =>
array (
0 => 'wmlc',
),
'wmlc' => 'application/vnd.wap.wmlc',
'application/vnd.wap.wmlscriptc' =>
array (
0 => 'wmlsc',
),
'wmlsc' => 'application/vnd.wap.wmlscriptc',
'application/vnd.webturbo' =>
array (
0 => 'wtb',
),
'wtb' => 'application/vnd.webturbo',
'application/vnd.wolfram.player' =>
array (
0 => 'nbp',
),
'nbp' => 'application/vnd.wolfram.player',
'application/vnd.wordperfect' =>
array (
0 => 'wpd',
),
'wpd' => 'application/vnd.wordperfect',
'application/vnd.wqd' =>
array (
0 => 'wqd',
),
'wqd' => 'application/vnd.wqd',
'application/vnd.wt.stf' =>
array (
0 => 'stf',
),
'stf' => 'application/vnd.wt.stf',
'application/vnd.xara' =>
array (
0 => 'xar',
),
'xar' => 'application/vnd.xara',
'application/vnd.xfdl' =>
array (
0 => 'xfdl',
),
'xfdl' => 'application/vnd.xfdl',
'application/vnd.yamaha.hv-dic' =>
array (
0 => 'hvd',
),
'hvd' => 'application/vnd.yamaha.hv-dic',
'application/vnd.yamaha.hv-script' =>
array (
0 => 'hvs',
),
'hvs' => 'application/vnd.yamaha.hv-script',
'application/vnd.yamaha.hv-voice' =>
array (
0 => 'hvp',
),
'hvp' => 'application/vnd.yamaha.hv-voice',
'application/vnd.yamaha.openscoreformat' =>
array (
0 => 'osf',
),
'osf' => 'application/vnd.yamaha.openscoreformat',
'application/vnd.yamaha.openscoreformat.osfpvg+xml' =>
array (
0 => 'osfpvg',
),
'osfpvg' => 'application/vnd.yamaha.openscoreformat.osfpvg+xml',
'application/vnd.yamaha.smaf-audio' =>
array (
0 => 'saf',
),
'saf' => 'application/vnd.yamaha.smaf-audio',
'application/vnd.yamaha.smaf-phrase' =>
array (
0 => 'spf',
),
'spf' => 'application/vnd.yamaha.smaf-phrase',
'application/vnd.yellowriver-custom-menu' =>
array (
0 => 'cmp',
),
'cmp' => 'application/vnd.yellowriver-custom-menu',
'application/vnd.zul' =>
array (
0 => 'zir',
1 => 'zirz',
),
'zir' => 'application/vnd.zul',
'zirz' => 'application/vnd.zul',
'application/vnd.zzazz.deck+xml' =>
array (
0 => 'zaz',
),
'zaz' => 'application/vnd.zzazz.deck+xml',
'application/voicexml+xml' =>
array (
0 => 'vxml',
),
'vxml' => 'application/voicexml+xml',
'application/wasm' =>
array (
0 => 'wasm',
),
'wasm' => 'application/wasm',
'application/widget' =>
array (
0 => 'wgt',
),
'wgt' => 'application/widget',
'application/winhlp' =>
array (
0 => 'hlp',
),
'hlp' => 'application/winhlp',
'application/wsdl+xml' =>
array (
0 => 'wsdl',
),
'wsdl' => 'application/wsdl+xml',
'application/wspolicy+xml' =>
array (
0 => 'wspolicy',
),
'wspolicy' => 'application/wspolicy+xml',
'application/x-7z-compressed' =>
array (
0 => '7z',
),
'7z' => 'application/x-7z-compressed',
'application/x-abiword' =>
array (
0 => 'abw',
),
'abw' => 'application/x-abiword',
'application/x-ace-compressed' =>
array (
0 => 'ace',
),
'ace' => 'application/x-ace-compressed',
'application/x-apple-diskimage' =>
array (
0 => 'dmg',
),
'dmg' => 'application/x-apple-diskimage',
'application/x-authorware-bin' =>
array (
0 => 'aab',
1 => 'x32',
2 => 'u32',
3 => 'vox',
),
'aab' => 'application/x-authorware-bin',
'x32' => 'application/x-authorware-bin',
'u32' => 'application/x-authorware-bin',
'vox' => 'application/x-authorware-bin',
'application/x-authorware-map' =>
array (
0 => 'aam',
),
'aam' => 'application/x-authorware-map',
'application/x-authorware-seg' =>
array (
0 => 'aas',
),
'aas' => 'application/x-authorware-seg',
'application/x-bcpio' =>
array (
0 => 'bcpio',
),
'bcpio' => 'application/x-bcpio',
'application/x-bittorrent' =>
array (
0 => 'torrent',
),
'torrent' => 'application/x-bittorrent',
'application/x-blorb' =>
array (
0 => 'blb',
1 => 'blorb',
),
'blb' => 'application/x-blorb',
'blorb' => 'application/x-blorb',
'application/x-bzip' =>
array (
0 => 'bz',
),
'bz' => 'application/x-bzip',
'application/x-bzip2' =>
array (
0 => 'bz2',
1 => 'boz',
),
'bz2' => 'application/x-bzip2',
'boz' => 'application/x-bzip2',
'application/x-cbr' =>
array (
0 => 'cbr',
1 => 'cba',
2 => 'cbt',
3 => 'cbz',
4 => 'cb7',
),
'cbr' => 'application/x-cbr',
'cba' => 'application/x-cbr',
'cbt' => 'application/x-cbr',
'cbz' => 'application/x-cbr',
'cb7' => 'application/x-cbr',
'application/x-cdlink' =>
array (
0 => 'vcd',
),
'vcd' => 'application/x-cdlink',
'application/x-cfs-compressed' =>
array (
0 => 'cfs',
),
'cfs' => 'application/x-cfs-compressed',
'application/x-chat' =>
array (
0 => 'chat',
),
'chat' => 'application/x-chat',
'application/x-chess-pgn' =>
array (
0 => 'pgn',
),
'pgn' => 'application/x-chess-pgn',
'application/x-conference' =>
array (
0 => 'nsc',
),
'nsc' => 'application/x-conference',
'application/x-cpio' =>
array (
0 => 'cpio',
),
'cpio' => 'application/x-cpio',
'application/x-csh' =>
array (
0 => 'csh',
),
'csh' => 'application/x-csh',
'application/x-debian-package' =>
array (
0 => 'deb',
1 => 'udeb',
),
'deb' => 'application/x-debian-package',
'udeb' => 'application/x-debian-package',
'application/x-dgc-compressed' =>
array (
0 => 'dgc',
),
'dgc' => 'application/x-dgc-compressed',
'application/x-director' =>
array (
0 => 'dir',
1 => 'dcr',
2 => 'dxr',
3 => 'cst',
4 => 'cct',
5 => 'cxt',
6 => 'w3d',
7 => 'fgd',
8 => 'swa',
),
'dir' => 'application/x-director',
'dcr' => 'application/x-director',
'dxr' => 'application/x-director',
'cst' => 'application/x-director',
'cct' => 'application/x-director',
'cxt' => 'application/x-director',
'w3d' => 'application/x-director',
'fgd' => 'application/x-director',
'swa' => 'application/x-director',
'application/x-doom' =>
array (
0 => 'wad',
),
'wad' => 'application/x-doom',
'application/x-dtbncx+xml' =>
array (
0 => 'ncx',
),
'ncx' => 'application/x-dtbncx+xml',
'application/x-dtbook+xml' =>
array (
0 => 'dtb',
),
'dtb' => 'application/x-dtbook+xml',
'application/x-dtbresource+xml' =>
array (
0 => 'res',
),
'res' => 'application/x-dtbresource+xml',
'application/x-dvi' =>
array (
0 => 'dvi',
),
'dvi' => 'application/x-dvi',
'application/x-envoy' =>
array (
0 => 'evy',
),
'evy' => 'application/x-envoy',
'application/x-eva' =>
array (
0 => 'eva',
),
'eva' => 'application/x-eva',
'application/x-font-bdf' =>
array (
0 => 'bdf',
),
'bdf' => 'application/x-font-bdf',
'application/x-font-ghostscript' =>
array (
0 => 'gsf',
),
'gsf' => 'application/x-font-ghostscript',
'application/x-font-linux-psf' =>
array (
0 => 'psf',
),
'psf' => 'application/x-font-linux-psf',
'application/x-font-pcf' =>
array (
0 => 'pcf',
),
'pcf' => 'application/x-font-pcf',
'application/x-font-snf' =>
array (
0 => 'snf',
),
'snf' => 'application/x-font-snf',
'application/x-font-type1' =>
array (
0 => 'pfa',
1 => 'pfb',
2 => 'pfm',
3 => 'afm',
),
'pfa' => 'application/x-font-type1',
'pfb' => 'application/x-font-type1',
'pfm' => 'application/x-font-type1',
'afm' => 'application/x-font-type1',
'application/x-freearc' =>
array (
0 => 'arc',
),
'arc' => 'application/x-freearc',
'application/x-futuresplash' =>
array (
0 => 'spl',
),
'spl' => 'application/x-futuresplash',
'application/x-gca-compressed' =>
array (
0 => 'gca',
),
'gca' => 'application/x-gca-compressed',
'application/x-glulx' =>
array (
0 => 'ulx',
),
'ulx' => 'application/x-glulx',
'application/x-gnumeric' =>
array (
0 => 'gnumeric',
),
'gnumeric' => 'application/x-gnumeric',
'application/x-gramps-xml' =>
array (
0 => 'gramps',
),
'gramps' => 'application/x-gramps-xml',
'application/x-gtar' =>
array (
0 => 'gtar',
),
'gtar' => 'application/x-gtar',
'application/x-hdf' =>
array (
0 => 'hdf',
),
'hdf' => 'application/x-hdf',
'application/x-install-instructions' =>
array (
0 => 'install',
),
'install' => 'application/x-install-instructions',
'application/x-iso9660-image' =>
array (
0 => 'iso',
),
'iso' => 'application/x-iso9660-image',
'application/x-java-jnlp-file' =>
array (
0 => 'jnlp',
),
'jnlp' => 'application/x-java-jnlp-file',
'application/x-latex' =>
array (
0 => 'latex',
),
'latex' => 'application/x-latex',
'application/x-lzh-compressed' =>
array (
0 => 'lzh',
1 => 'lha',
),
'lzh' => 'application/x-lzh-compressed',
'lha' => 'application/x-lzh-compressed',
'application/x-mie' =>
array (
0 => 'mie',
),
'mie' => 'application/x-mie',
'application/x-mobipocket-ebook' =>
array (
0 => 'prc',
1 => 'mobi',
),
'prc' => 'application/x-mobipocket-ebook',
'mobi' => 'application/x-mobipocket-ebook',
'application/x-ms-application' =>
array (
0 => 'application',
),
'application' => 'application/x-ms-application',
'application/x-ms-shortcut' =>
array (
0 => 'lnk',
),
'lnk' => 'application/x-ms-shortcut',
'application/x-ms-wmd' =>
array (
0 => 'wmd',
),
'wmd' => 'application/x-ms-wmd',
'application/x-ms-wmz' =>
array (
0 => 'wmz',
),
'wmz' => 'application/x-msmetafile',
'application/x-ms-xbap' =>
array (
0 => 'xbap',
),
'xbap' => 'application/x-ms-xbap',
'application/x-msaccess' =>
array (
0 => 'mdb',
),
'mdb' => 'application/x-msaccess',
'application/x-msbinder' =>
array (
0 => 'obd',
),
'obd' => 'application/x-msbinder',
'application/x-mscardfile' =>
array (
0 => 'crd',
),
'crd' => 'application/x-mscardfile',
'application/x-msclip' =>
array (
0 => 'clp',
),
'clp' => 'application/x-msclip',
'application/x-msdownload' =>
array (
0 => 'exe',
1 => 'dll',
2 => 'com',
3 => 'bat',
4 => 'msi',
),
'exe' => 'application/x-msdownload',
'dll' => 'application/x-msdownload',
'com' => 'application/x-msdownload',
'bat' => 'application/x-msdownload',
'msi' => 'application/x-msdownload',
'application/x-msmediaview' =>
array (
0 => 'mvb',
1 => 'm13',
2 => 'm14',
),
'mvb' => 'application/x-msmediaview',
'm13' => 'application/x-msmediaview',
'm14' => 'application/x-msmediaview',
'application/x-msmetafile' =>
array (
0 => 'wmf',
1 => 'wmz',
2 => 'emf',
3 => 'emz',
),
'wmf' => 'application/x-msmetafile',
'emf' => 'application/x-msmetafile',
'emz' => 'application/x-msmetafile',
'application/x-msmoney' =>
array (
0 => 'mny',
),
'mny' => 'application/x-msmoney',
'application/x-mspublisher' =>
array (
0 => 'pub',
),
'pub' => 'application/x-mspublisher',
'application/x-msschedule' =>
array (
0 => 'scd',
),
'scd' => 'application/x-msschedule',
'application/x-msterminal' =>
array (
0 => 'trm',
),
'trm' => 'application/x-msterminal',
'application/x-mswrite' =>
array (
0 => 'wri',
),
'wri' => 'application/x-mswrite',
'application/x-netcdf' =>
array (
0 => 'nc',
1 => 'cdf',
),
'nc' => 'application/x-netcdf',
'cdf' => 'application/x-netcdf',
'application/x-nzb' =>
array (
0 => 'nzb',
),
'nzb' => 'application/x-nzb',
'application/x-pkcs12' =>
array (
0 => 'p12',
1 => 'pfx',
),
'p12' => 'application/x-pkcs12',
'pfx' => 'application/x-pkcs12',
'application/x-pkcs7-certificates' =>
array (
0 => 'p7b',
1 => 'spc',
),
'p7b' => 'application/x-pkcs7-certificates',
'spc' => 'application/x-pkcs7-certificates',
'application/x-pkcs7-certreqresp' =>
array (
0 => 'p7r',
),
'p7r' => 'application/x-pkcs7-certreqresp',
'application/x-rar-compressed' =>
array (
0 => 'rar',
),
'rar' => 'application/x-rar-compressed',
'application/x-research-info-systems' =>
array (
0 => 'ris',
),
'ris' => 'application/x-research-info-systems',
'application/x-sh' =>
array (
0 => 'sh',
),
'sh' => 'application/x-sh',
'application/x-shar' =>
array (
0 => 'shar',
),
'shar' => 'application/x-shar',
'application/x-shockwave-flash' =>
array (
0 => 'swf',
),
'swf' => 'application/x-shockwave-flash',
'application/x-silverlight-app' =>
array (
0 => 'xap',
),
'xap' => 'application/x-silverlight-app',
'application/x-sql' =>
array (
0 => 'sql',
),
'sql' => 'application/x-sql',
'application/x-stuffit' =>
array (
0 => 'sit',
),
'sit' => 'application/x-stuffit',
'application/x-stuffitx' =>
array (
0 => 'sitx',
),
'sitx' => 'application/x-stuffitx',
'application/x-subrip' =>
array (
0 => 'srt',
),
'srt' => 'application/x-subrip',
'application/x-sv4cpio' =>
array (
0 => 'sv4cpio',
),
'sv4cpio' => 'application/x-sv4cpio',
'application/x-sv4crc' =>
array (
0 => 'sv4crc',
),
'sv4crc' => 'application/x-sv4crc',
'application/x-t3vm-image' =>
array (
0 => 't3',
),
't3' => 'application/x-t3vm-image',
'application/x-tads' =>
array (
0 => 'gam',
),
'gam' => 'application/x-tads',
'application/x-tar' =>
array (
0 => 'tar',
),
'tar' => 'application/x-tar',
'application/x-tcl' =>
array (
0 => 'tcl',
),
'tcl' => 'application/x-tcl',
'application/x-tex' =>
array (
0 => 'tex',
),
'tex' => 'application/x-tex',
'application/x-tex-tfm' =>
array (
0 => 'tfm',
),
'tfm' => 'application/x-tex-tfm',
'application/x-texinfo' =>
array (
0 => 'texinfo',
1 => 'texi',
),
'texinfo' => 'application/x-texinfo',
'texi' => 'application/x-texinfo',
'application/x-tgif' =>
array (
0 => 'obj',
),
'obj' => 'application/x-tgif',
'application/x-ustar' =>
array (
0 => 'ustar',
),
'ustar' => 'application/x-ustar',
'application/x-wais-source' =>
array (
0 => 'src',
),
'src' => 'application/x-wais-source',
'application/x-x509-ca-cert' =>
array (
0 => 'der',
1 => 'crt',
),
'der' => 'application/x-x509-ca-cert',
'crt' => 'application/x-x509-ca-cert',
'application/x-xfig' =>
array (
0 => 'fig',
),
'fig' => 'application/x-xfig',
'application/x-xliff+xml' =>
array (
0 => 'xlf',
),
'xlf' => 'application/x-xliff+xml',
'application/x-xpinstall' =>
array (
0 => 'xpi',
),
'xpi' => 'application/x-xpinstall',
'application/x-xz' =>
array (
0 => 'xz',
),
'xz' => 'application/x-xz',
'application/x-zmachine' =>
array (
0 => 'z1',
1 => 'z2',
2 => 'z3',
3 => 'z4',
4 => 'z5',
5 => 'z6',
6 => 'z7',
7 => 'z8',
),
'z1' => 'application/x-zmachine',
'z2' => 'application/x-zmachine',
'z3' => 'application/x-zmachine',
'z4' => 'application/x-zmachine',
'z5' => 'application/x-zmachine',
'z6' => 'application/x-zmachine',
'z7' => 'application/x-zmachine',
'z8' => 'application/x-zmachine',
'application/xaml+xml' =>
array (
0 => 'xaml',
),
'xaml' => 'application/xaml+xml',
'application/xcap-diff+xml' =>
array (
0 => 'xdf',
),
'xdf' => 'application/xcap-diff+xml',
'application/xenc+xml' =>
array (
0 => 'xenc',
),
'xenc' => 'application/xenc+xml',
'application/xhtml+xml' =>
array (
0 => 'xhtml',
1 => 'xht',
),
'xhtml' => 'application/xhtml+xml',
'xht' => 'application/xhtml+xml',
'application/xml' =>
array (
0 => 'xml',
1 => 'xsl',
),
'xml' => 'application/xml',
'xsl' => 'application/xml',
'application/xml-dtd' =>
array (
0 => 'dtd',
),
'dtd' => 'application/xml-dtd',
'application/xop+xml' =>
array (
0 => 'xop',
),
'xop' => 'application/xop+xml',
'application/xproc+xml' =>
array (
0 => 'xpl',
),
'xpl' => 'application/xproc+xml',
'application/xslt+xml' =>
array (
0 => 'xslt',
),
'xslt' => 'application/xslt+xml',
'application/xspf+xml' =>
array (
0 => 'xspf',
),
'xspf' => 'application/xspf+xml',
'application/xv+xml' =>
array (
0 => 'mxml',
1 => 'xhvml',
2 => 'xvml',
3 => 'xvm',
),
'mxml' => 'application/xv+xml',
'xhvml' => 'application/xv+xml',
'xvml' => 'application/xv+xml',
'xvm' => 'application/xv+xml',
'application/yang' =>
array (
0 => 'yang',
),
'yang' => 'application/yang',
'application/yin+xml' =>
array (
0 => 'yin',
),
'yin' => 'application/yin+xml',
'application/zip' =>
array (
0 => 'zip',
),
'zip' => 'application/zip',
'audio/adpcm' =>
array (
0 => 'adp',
),
'adp' => 'audio/adpcm',
'audio/basic' =>
array (
0 => 'au',
1 => 'snd',
),
'au' => 'audio/basic',
'snd' => 'audio/basic',
'audio/midi' =>
array (
0 => 'mid',
1 => 'midi',
2 => 'kar',
3 => 'rmi',
),
'mid' => 'audio/midi',
'midi' => 'audio/midi',
'kar' => 'audio/midi',
'rmi' => 'audio/midi',
'audio/mp4' =>
array (
0 => 'm4a',
1 => 'mp4a',
),
'm4a' => 'audio/mp4',
'mp4a' => 'audio/mp4',
'audio/mpeg' =>
array (
0 => 'mpga',
1 => 'mp2',
2 => 'mp2a',
3 => 'mp3',
4 => 'm2a',
5 => 'm3a',
),
'mpga' => 'audio/mpeg',
'mp2' => 'audio/mpeg',
'mp2a' => 'audio/mpeg',
'mp3' => 'audio/mpeg',
'm2a' => 'audio/mpeg',
'm3a' => 'audio/mpeg',
'audio/ogg' =>
array (
0 => 'oga',
1 => 'ogg',
2 => 'spx',
3 => 'opus',
),
'oga' => 'audio/ogg',
'ogg' => 'audio/ogg',
'spx' => 'audio/ogg',
'opus' => 'audio/ogg',
'audio/s3m' =>
array (
0 => 's3m',
),
's3m' => 'audio/s3m',
'audio/silk' =>
array (
0 => 'sil',
),
'sil' => 'audio/silk',
'audio/vnd.dece.audio' =>
array (
0 => 'uva',
1 => 'uvva',
),
'uva' => 'audio/vnd.dece.audio',
'uvva' => 'audio/vnd.dece.audio',
'audio/vnd.digital-winds' =>
array (
0 => 'eol',
),
'eol' => 'audio/vnd.digital-winds',
'audio/vnd.dra' =>
array (
0 => 'dra',
),
'dra' => 'audio/vnd.dra',
'audio/vnd.dts' =>
array (
0 => 'dts',
),
'dts' => 'audio/vnd.dts',
'audio/vnd.dts.hd' =>
array (
0 => 'dtshd',
),
'dtshd' => 'audio/vnd.dts.hd',
'audio/vnd.lucent.voice' =>
array (
0 => 'lvp',
),
'lvp' => 'audio/vnd.lucent.voice',
'audio/vnd.ms-playready.media.pya' =>
array (
0 => 'pya',
),
'pya' => 'audio/vnd.ms-playready.media.pya',
'audio/vnd.nuera.ecelp4800' =>
array (
0 => 'ecelp4800',
),
'ecelp4800' => 'audio/vnd.nuera.ecelp4800',
'audio/vnd.nuera.ecelp7470' =>
array (
0 => 'ecelp7470',
),
'ecelp7470' => 'audio/vnd.nuera.ecelp7470',
'audio/vnd.nuera.ecelp9600' =>
array (
0 => 'ecelp9600',
),
'ecelp9600' => 'audio/vnd.nuera.ecelp9600',
'audio/vnd.rip' =>
array (
0 => 'rip',
),
'rip' => 'audio/vnd.rip',
'audio/webm' =>
array (
0 => 'weba',
),
'weba' => 'audio/webm',
'audio/x-aac' =>
array (
0 => 'aac',
),
'aac' => 'audio/x-aac',
'audio/x-aiff' =>
array (
0 => 'aif',
1 => 'aiff',
2 => 'aifc',
),
'aif' => 'audio/x-aiff',
'aiff' => 'audio/x-aiff',
'aifc' => 'audio/x-aiff',
'audio/x-caf' =>
array (
0 => 'caf',
),
'caf' => 'audio/x-caf',
'audio/x-flac' =>
array (
0 => 'flac',
),
'flac' => 'audio/x-flac',
'audio/x-matroska' =>
array (
0 => 'mka',
),
'mka' => 'audio/x-matroska',
'audio/x-mpegurl' =>
array (
0 => 'm3u',
),
'm3u' => 'audio/x-mpegurl',
'audio/x-ms-wax' =>
array (
0 => 'wax',
),
'wax' => 'audio/x-ms-wax',
'audio/x-ms-wma' =>
array (
0 => 'wma',
),
'wma' => 'audio/x-ms-wma',
'audio/x-pn-realaudio' =>
array (
0 => 'ram',
1 => 'ra',
),
'ram' => 'audio/x-pn-realaudio',
'ra' => 'audio/x-pn-realaudio',
'audio/x-pn-realaudio-plugin' =>
array (
0 => 'rmp',
),
'rmp' => 'audio/x-pn-realaudio-plugin',
'audio/x-wav' =>
array (
0 => 'wav',
),
'wav' => 'audio/x-wav',
'audio/xm' =>
array (
0 => 'xm',
),
'xm' => 'audio/xm',
'chemical/x-cdx' =>
array (
0 => 'cdx',
),
'cdx' => 'chemical/x-cdx',
'chemical/x-cif' =>
array (
0 => 'cif',
),
'cif' => 'chemical/x-cif',
'chemical/x-cmdf' =>
array (
0 => 'cmdf',
),
'cmdf' => 'chemical/x-cmdf',
'chemical/x-cml' =>
array (
0 => 'cml',
),
'cml' => 'chemical/x-cml',
'chemical/x-csml' =>
array (
0 => 'csml',
),
'csml' => 'chemical/x-csml',
'chemical/x-xyz' =>
array (
0 => 'xyz',
),
'xyz' => 'chemical/x-xyz',
'font/collection' =>
array (
0 => 'ttc',
),
'ttc' => 'font/collection',
'font/otf' =>
array (
0 => 'otf',
),
'otf' => 'font/otf',
'font/ttf' =>
array (
0 => 'ttf',
),
'ttf' => 'font/ttf',
'font/woff' =>
array (
0 => 'woff',
),
'woff' => 'font/woff',
'font/woff2' =>
array (
0 => 'woff2',
),
'woff2' => 'font/woff2',
'image/bmp' =>
array (
0 => 'bmp',
),
'bmp' => 'image/bmp',
'image/cgm' =>
array (
0 => 'cgm',
),
'cgm' => 'image/cgm',
'image/g3fax' =>
array (
0 => 'g3',
),
'g3' => 'image/g3fax',
'image/gif' =>
array (
0 => 'gif',
),
'gif' => 'image/gif',
'image/ief' =>
array (
0 => 'ief',
),
'ief' => 'image/ief',
'image/jpeg' =>
array (
0 => 'jpeg',
1 => 'jpg',
2 => 'jpe',
),
'jpeg' => 'image/jpeg',
'jpg' => 'image/jpeg',
'jpe' => 'image/jpeg',
'image/ktx' =>
array (
0 => 'ktx',
),
'ktx' => 'image/ktx',
'image/png' =>
array (
0 => 'png',
),
'png' => 'image/png',
'image/prs.btif' =>
array (
0 => 'btif',
),
'btif' => 'image/prs.btif',
'image/sgi' =>
array (
0 => 'sgi',
),
'sgi' => 'image/sgi',
'image/svg+xml' =>
array (
0 => 'svg',
1 => 'svgz',
),
'svg' => 'image/svg+xml',
'svgz' => 'image/svg+xml',
'image/tiff' =>
array (
0 => 'tiff',
1 => 'tif',
),
'tiff' => 'image/tiff',
'tif' => 'image/tiff',
'image/vnd.adobe.photoshop' =>
array (
0 => 'psd',
),
'psd' => 'image/vnd.adobe.photoshop',
'image/vnd.dece.graphic' =>
array (
0 => 'uvi',
1 => 'uvvi',
2 => 'uvg',
3 => 'uvvg',
),
'uvi' => 'image/vnd.dece.graphic',
'uvvi' => 'image/vnd.dece.graphic',
'uvg' => 'image/vnd.dece.graphic',
'uvvg' => 'image/vnd.dece.graphic',
'image/vnd.djvu' =>
array (
0 => 'djvu',
1 => 'djv',
),
'djvu' => 'image/vnd.djvu',
'djv' => 'image/vnd.djvu',
'image/vnd.dvb.subtitle' =>
array (
0 => 'sub',
),
'sub' => 'text/vnd.dvb.subtitle',
'image/vnd.dwg' =>
array (
0 => 'dwg',
),
'dwg' => 'image/vnd.dwg',
'image/vnd.dxf' =>
array (
0 => 'dxf',
),
'dxf' => 'image/vnd.dxf',
'image/vnd.fastbidsheet' =>
array (
0 => 'fbs',
),
'fbs' => 'image/vnd.fastbidsheet',
'image/vnd.fpx' =>
array (
0 => 'fpx',
),
'fpx' => 'image/vnd.fpx',
'image/vnd.fst' =>
array (
0 => 'fst',
),
'fst' => 'image/vnd.fst',
'image/vnd.fujixerox.edmics-mmr' =>
array (
0 => 'mmr',
),
'mmr' => 'image/vnd.fujixerox.edmics-mmr',
'image/vnd.fujixerox.edmics-rlc' =>
array (
0 => 'rlc',
),
'rlc' => 'image/vnd.fujixerox.edmics-rlc',
'image/vnd.ms-modi' =>
array (
0 => 'mdi',
),
'mdi' => 'image/vnd.ms-modi',
'image/vnd.ms-photo' =>
array (
0 => 'wdp',
),
'wdp' => 'image/vnd.ms-photo',
'image/vnd.net-fpx' =>
array (
0 => 'npx',
),
'npx' => 'image/vnd.net-fpx',
'image/vnd.wap.wbmp' =>
array (
0 => 'wbmp',
),
'wbmp' => 'image/vnd.wap.wbmp',
'image/vnd.xiff' =>
array (
0 => 'xif',
),
'xif' => 'image/vnd.xiff',
'image/webp' =>
array (
0 => 'webp',
),
'webp' => 'image/webp',
'image/x-3ds' =>
array (
0 => '3ds',
),
'3ds' => 'image/x-3ds',
'image/x-cmu-raster' =>
array (
0 => 'ras',
),
'ras' => 'image/x-cmu-raster',
'image/x-cmx' =>
array (
0 => 'cmx',
),
'cmx' => 'image/x-cmx',
'image/x-freehand' =>
array (
0 => 'fh',
1 => 'fhc',
2 => 'fh4',
3 => 'fh5',
4 => 'fh7',
),
'fh' => 'image/x-freehand',
'fhc' => 'image/x-freehand',
'fh4' => 'image/x-freehand',
'fh5' => 'image/x-freehand',
'fh7' => 'image/x-freehand',
'image/x-icon' =>
array (
0 => 'ico',
),
'ico' => 'image/x-icon',
'image/x-mrsid-image' =>
array (
0 => 'sid',
),
'sid' => 'image/x-mrsid-image',
'image/x-pcx' =>
array (
0 => 'pcx',
),
'pcx' => 'image/x-pcx',
'image/x-pict' =>
array (
0 => 'pic',
1 => 'pct',
),
'pic' => 'image/x-pict',
'pct' => 'image/x-pict',
'image/x-portable-anymap' =>
array (
0 => 'pnm',
),
'pnm' => 'image/x-portable-anymap',
'image/x-portable-bitmap' =>
array (
0 => 'pbm',
),
'pbm' => 'image/x-portable-bitmap',
'image/x-portable-graymap' =>
array (
0 => 'pgm',
),
'pgm' => 'image/x-portable-graymap',
'image/x-portable-pixmap' =>
array (
0 => 'ppm',
),
'ppm' => 'image/x-portable-pixmap',
'image/x-rgb' =>
array (
0 => 'rgb',
),
'rgb' => 'image/x-rgb',
'image/x-tga' =>
array (
0 => 'tga',
),
'tga' => 'image/x-tga',
'image/x-xbitmap' =>
array (
0 => 'xbm',
),
'xbm' => 'image/x-xbitmap',
'image/x-xpixmap' =>
array (
0 => 'xpm',
),
'xpm' => 'image/x-xpixmap',
'image/x-xwindowdump' =>
array (
0 => 'xwd',
),
'xwd' => 'image/x-xwindowdump',
'message/rfc822' =>
array (
0 => 'eml',
1 => 'mime',
),
'eml' => 'message/rfc822',
'mime' => 'message/rfc822',
'model/iges' =>
array (
0 => 'igs',
1 => 'iges',
),
'igs' => 'model/iges',
'iges' => 'model/iges',
'model/mesh' =>
array (
0 => 'msh',
1 => 'mesh',
2 => 'silo',
),
'msh' => 'model/mesh',
'mesh' => 'model/mesh',
'silo' => 'model/mesh',
'model/vnd.collada+xml' =>
array (
0 => 'dae',
),
'dae' => 'model/vnd.collada+xml',
'model/vnd.dwf' =>
array (
0 => 'dwf',
),
'dwf' => 'model/vnd.dwf',
'model/vnd.gdl' =>
array (
0 => 'gdl',
),
'gdl' => 'model/vnd.gdl',
'model/vnd.gtw' =>
array (
0 => 'gtw',
),
'gtw' => 'model/vnd.gtw',
'model/vnd.mts' =>
array (
0 => 'mts',
),
'mts' => 'model/vnd.mts',
'model/vnd.vtu' =>
array (
0 => 'vtu',
),
'vtu' => 'model/vnd.vtu',
'model/vrml' =>
array (
0 => 'wrl',
1 => 'vrml',
),
'wrl' => 'model/vrml',
'vrml' => 'model/vrml',
'model/x3d+binary' =>
array (
0 => 'x3db',
1 => 'x3dbz',
),
'x3db' => 'model/x3d+binary',
'x3dbz' => 'model/x3d+binary',
'model/x3d+vrml' =>
array (
0 => 'x3dv',
1 => 'x3dvz',
),
'x3dv' => 'model/x3d+vrml',
'x3dvz' => 'model/x3d+vrml',
'model/x3d+xml' =>
array (
0 => 'x3d',
1 => 'x3dz',
),
'x3d' => 'model/x3d+xml',
'x3dz' => 'model/x3d+xml',
'text/cache-manifest' =>
array (
0 => 'appcache',
),
'appcache' => 'text/cache-manifest',
'text/calendar' =>
array (
0 => 'ics',
1 => 'ifb',
),
'ics' => 'text/calendar',
'ifb' => 'text/calendar',
'text/css' =>
array (
0 => 'css',
),
'css' => 'text/css',
'text/csv' =>
array (
0 => 'csv',
),
'csv' => 'text/csv',
'text/html' =>
array (
0 => 'html',
1 => 'htm',
),
'html' => 'text/html',
'htm' => 'text/html',
'text/javascript' =>
array (
0 => 'js',
1 => 'mjs',
),
'js' => 'text/javascript',
'mjs' => 'text/javascript',
'text/n3' =>
array (
0 => 'n3',
),
'n3' => 'text/n3',
'text/plain' =>
array (
0 => 'txt',
1 => 'text',
2 => 'conf',
3 => 'def',
4 => 'list',
5 => 'log',
6 => 'in',
),
'txt' => 'text/plain',
'text' => 'text/plain',
'conf' => 'text/plain',
'def' => 'text/plain',
'list' => 'text/plain',
'log' => 'text/plain',
'in' => 'text/plain',
'text/prs.lines.tag' =>
array (
0 => 'dsc',
),
'dsc' => 'text/prs.lines.tag',
'text/richtext' =>
array (
0 => 'rtx',
),
'rtx' => 'text/richtext',
'text/sgml' =>
array (
0 => 'sgml',
1 => 'sgm',
),
'sgml' => 'text/sgml',
'sgm' => 'text/sgml',
'text/tab-separated-values' =>
array (
0 => 'tsv',
),
'tsv' => 'text/tab-separated-values',
'text/troff' =>
array (
0 => 't',
1 => 'tr',
2 => 'roff',
3 => 'man',
4 => 'me',
5 => 'ms',
),
't' => 'text/troff',
'tr' => 'text/troff',
'roff' => 'text/troff',
'man' => 'text/troff',
'me' => 'text/troff',
'ms' => 'text/troff',
'text/turtle' =>
array (
0 => 'ttl',
),
'ttl' => 'text/turtle',
'text/uri-list' =>
array (
0 => 'uri',
1 => 'uris',
2 => 'urls',
),
'uri' => 'text/uri-list',
'uris' => 'text/uri-list',
'urls' => 'text/uri-list',
'text/vcard' =>
array (
0 => 'vcard',
),
'vcard' => 'text/vcard',
'text/vnd.curl' =>
array (
0 => 'curl',
),
'curl' => 'text/vnd.curl',
'text/vnd.curl.dcurl' =>
array (
0 => 'dcurl',
),
'dcurl' => 'text/vnd.curl.dcurl',
'text/vnd.curl.mcurl' =>
array (
0 => 'mcurl',
),
'mcurl' => 'text/vnd.curl.mcurl',
'text/vnd.curl.scurl' =>
array (
0 => 'scurl',
),
'scurl' => 'text/vnd.curl.scurl',
'text/vnd.dvb.subtitle' =>
array (
0 => 'sub',
),
'text/vnd.fly' =>
array (
0 => 'fly',
),
'fly' => 'text/vnd.fly',
'text/vnd.fmi.flexstor' =>
array (
0 => 'flx',
),
'flx' => 'text/vnd.fmi.flexstor',
'text/vnd.graphviz' =>
array (
0 => 'gv',
),
'gv' => 'text/vnd.graphviz',
'text/vnd.in3d.3dml' =>
array (
0 => '3dml',
),
'3dml' => 'text/vnd.in3d.3dml',
'text/vnd.in3d.spot' =>
array (
0 => 'spot',
),
'spot' => 'text/vnd.in3d.spot',
'text/vnd.sun.j2me.app-descriptor' =>
array (
0 => 'jad',
),
'jad' => 'text/vnd.sun.j2me.app-descriptor',
'text/vnd.wap.wml' =>
array (
0 => 'wml',
),
'wml' => 'text/vnd.wap.wml',
'text/vnd.wap.wmlscript' =>
array (
0 => 'wmls',
),
'wmls' => 'text/vnd.wap.wmlscript',
'text/x-asm' =>
array (
0 => 's',
1 => 'asm',
),
's' => 'text/x-asm',
'asm' => 'text/x-asm',
'text/x-c' =>
array (
0 => 'c',
1 => 'cc',
2 => 'cxx',
3 => 'cpp',
4 => 'h',
5 => 'hh',
6 => 'dic',
),
'c' => 'text/x-c',
'cc' => 'text/x-c',
'cxx' => 'text/x-c',
'cpp' => 'text/x-c',
'h' => 'text/x-c',
'hh' => 'text/x-c',
'dic' => 'text/x-c',
'text/x-fortran' =>
array (
0 => 'f',
1 => 'for',
2 => 'f77',
3 => 'f90',
),
'f' => 'text/x-fortran',
'for' => 'text/x-fortran',
'f77' => 'text/x-fortran',
'f90' => 'text/x-fortran',
'text/x-java-source' =>
array (
0 => 'java',
),
'java' => 'text/x-java-source',
'text/x-nfo' =>
array (
0 => 'nfo',
),
'nfo' => 'text/x-nfo',
'text/x-opml' =>
array (
0 => 'opml',
),
'opml' => 'text/x-opml',
'text/x-pascal' =>
array (
0 => 'p',
1 => 'pas',
),
'p' => 'text/x-pascal',
'pas' => 'text/x-pascal',
'text/x-setext' =>
array (
0 => 'etx',
),
'etx' => 'text/x-setext',
'text/x-sfv' =>
array (
0 => 'sfv',
),
'sfv' => 'text/x-sfv',
'text/x-uuencode' =>
array (
0 => 'uu',
),
'uu' => 'text/x-uuencode',
'text/x-vcalendar' =>
array (
0 => 'vcs',
),
'vcs' => 'text/x-vcalendar',
'text/x-vcard' =>
array (
0 => 'vcf',
),
'vcf' => 'text/x-vcard',
'video/3gpp' =>
array (
0 => '3gp',
),
'3gp' => 'video/3gpp',
'video/3gpp2' =>
array (
0 => '3g2',
),
'3g2' => 'video/3gpp2',
'video/h261' =>
array (
0 => 'h261',
),
'h261' => 'video/h261',
'video/h263' =>
array (
0 => 'h263',
),
'h263' => 'video/h263',
'video/h264' =>
array (
0 => 'h264',
),
'h264' => 'video/h264',
'video/jpeg' =>
array (
0 => 'jpgv',
),
'jpgv' => 'video/jpeg',
'video/jpm' =>
array (
0 => 'jpm',
1 => 'jpgm',
),
'jpm' => 'video/jpm',
'jpgm' => 'video/jpm',
'video/mj2' =>
array (
0 => 'mj2',
1 => 'mjp2',
),
'mj2' => 'video/mj2',
'mjp2' => 'video/mj2',
'video/mp4' =>
array (
0 => 'mp4',
1 => 'mp4v',
2 => 'mpg4',
),
'mp4' => 'video/mp4',
'mp4v' => 'video/mp4',
'mpg4' => 'video/mp4',
'video/mpeg' =>
array (
0 => 'mpeg',
1 => 'mpg',
2 => 'mpe',
3 => 'm1v',
4 => 'm2v',
),
'mpeg' => 'video/mpeg',
'mpg' => 'video/mpeg',
'mpe' => 'video/mpeg',
'm1v' => 'video/mpeg',
'm2v' => 'video/mpeg',
'video/ogg' =>
array (
0 => 'ogv',
),
'ogv' => 'video/ogg',
'video/quicktime' =>
array (
0 => 'qt',
1 => 'mov',
),
'qt' => 'video/quicktime',
'mov' => 'video/quicktime',
'video/vnd.dece.hd' =>
array (
0 => 'uvh',
1 => 'uvvh',
),
'uvh' => 'video/vnd.dece.hd',
'uvvh' => 'video/vnd.dece.hd',
'video/vnd.dece.mobile' =>
array (
0 => 'uvm',
1 => 'uvvm',
),
'uvm' => 'video/vnd.dece.mobile',
'uvvm' => 'video/vnd.dece.mobile',
'video/vnd.dece.pd' =>
array (
0 => 'uvp',
1 => 'uvvp',
),
'uvp' => 'video/vnd.dece.pd',
'uvvp' => 'video/vnd.dece.pd',
'video/vnd.dece.sd' =>
array (
0 => 'uvs',
1 => 'uvvs',
),
'uvs' => 'video/vnd.dece.sd',
'uvvs' => 'video/vnd.dece.sd',
'video/vnd.dece.video' =>
array (
0 => 'uvv',
1 => 'uvvv',
),
'uvv' => 'video/vnd.dece.video',
'uvvv' => 'video/vnd.dece.video',
'video/vnd.dvb.file' =>
array (
0 => 'dvb',
),
'dvb' => 'video/vnd.dvb.file',
'video/vnd.fvt' =>
array (
0 => 'fvt',
),
'fvt' => 'video/vnd.fvt',
'video/vnd.mpegurl' =>
array (
0 => 'mxu',
1 => 'm4u',
),
'mxu' => 'video/vnd.mpegurl',
'm4u' => 'video/vnd.mpegurl',
'video/vnd.ms-playready.media.pyv' =>
array (
0 => 'pyv',
),
'pyv' => 'video/vnd.ms-playready.media.pyv',
'video/vnd.uvvu.mp4' =>
array (
0 => 'uvu',
1 => 'uvvu',
),
'uvu' => 'video/vnd.uvvu.mp4',
'uvvu' => 'video/vnd.uvvu.mp4',
'video/vnd.vivo' =>
array (
0 => 'viv',
),
'viv' => 'video/vnd.vivo',
'video/webm' =>
array (
0 => 'webm',
),
'webm' => 'video/webm',
'video/x-f4v' =>
array (
0 => 'f4v',
),
'f4v' => 'video/x-f4v',
'video/x-fli' =>
array (
0 => 'fli',
),
'fli' => 'video/x-fli',
'video/x-flv' =>
array (
0 => 'flv',
),
'flv' => 'video/x-flv',
'video/x-m4v' =>
array (
0 => 'm4v',
),
'm4v' => 'video/x-m4v',
'video/x-matroska' =>
array (
0 => 'mkv',
1 => 'mk3d',
2 => 'mks',
),
'mkv' => 'video/x-matroska',
'mk3d' => 'video/x-matroska',
'mks' => 'video/x-matroska',
'video/x-mng' =>
array (
0 => 'mng',
),
'mng' => 'video/x-mng',
'video/x-ms-asf' =>
array (
0 => 'asf',
1 => 'asx',
),
'asf' => 'video/x-ms-asf',
'asx' => 'video/x-ms-asf',
'video/x-ms-vob' =>
array (
0 => 'vob',
),
'vob' => 'video/x-ms-vob',
'video/x-ms-wm' =>
array (
0 => 'wm',
),
'wm' => 'video/x-ms-wm',
'video/x-ms-wmv' =>
array (
0 => 'wmv',
),
'wmv' => 'video/x-ms-wmv',
'video/x-ms-wmx' =>
array (
0 => 'wmx',
),
'wmx' => 'video/x-ms-wmx',
'video/x-ms-wvx' =>
array (
0 => 'wvx',
),
'wvx' => 'video/x-ms-wvx',
'video/x-msvideo' =>
array (
0 => 'avi',
),
'avi' => 'video/x-msvideo',
'video/x-sgi-movie' =>
array (
0 => 'movie',
),
'movie' => 'video/x-sgi-movie',
'video/x-smv' =>
array (
0 => 'smv',
),
'smv' => 'video/x-smv',
'x-conference/x-cooltalk' =>
array (
0 => 'ice',
),
'ice' => 'x-conference/x-cooltalk',
)
?>