<?php

// Required: php5.2+
date_default_timezone_set("UTC");
// SET TO ZERO ONCE DONE DEVELOPING
define("CONF_HROUTER_DEPLOY", 1);
// SET TO ZERO ONCE DONE DEVELOPING
// Currently used for error 404:
define("CONF_HROUTER_DEBUG", 1);
// External Echo/Print will not work properly during ServeFile()
// IF you want to print debug messages or similar, then must be 1 or true.
// Otherwise it can be 0 if you are not going to echo/print.
define("CONF_HROUTER_USE_OBLEN", 1);
// Set whether any $_FILES will have pathinfo property:
define("CONF_HROUTER_FILES_PATHINFO", 1);
// When parsing, always trim last URI slashes:
define("CONF_HROUTER_RMLS", 1);


// TO INSTALL JUST RUN VIA WEB BROWSER:
// example: http://localhost/.php

// NOT REQUIRED TO EDIT ANYTHING BELOW!

define("CONF_HROUTER_PATH", dirname(__FILE__));
define("CONF_HROUTER_FILENAME", pathinfo(__FILE__)["filename"]);
define("CONF_HROUTER_FULLFILENAME", CONF_HROUTER_FILENAME.".php");
define("CONF_HROUTER_DEPLOYFILENAME", CONF_HROUTER_FILENAME.".json");
define("CONF_HROUTER_DEPLOYMIMESFILENAME", "mimes.php");
define("CONF_HROUTER_PAGESDIRNAME", "pages");
define("CONF_HROUTER_MODSMAINDIRNAME", "mods");
define("CONF_HROUTER_MODSSUBDIRNAME", "mods/etc");

// AUTO INSTALL?
Hrouter::Install(1); // 1=auto, no args=force

// Allocate mime list
$MimeListGlobal = array();
if (file_exists(CONF_HROUTER_DEPLOYMIMESFILENAME))
	require_once CONF_HROUTER_DEPLOYMIMESFILENAME;

// Prepare class for use:
Hrouter::__Main__(CONF_HROUTER_PATH);
// Prepare solution:
Hrouter::FindRouteSolution();
// GetVValues()[0=filename, 1=ext, 2=mvalue, 3=mvalueA, 4=VPath, 5=RPath, 6=uriPath]
// GetVValues()[FileName, FileExt, V, VA, VPath, RPath, Path]
define("PageFileName", PageVV(0));
define("PageFileExt", PageVV(1));
define("PageRPath", PageVV(5));
define("PagePath", PageVV(6));
// Finalize output
Hrouter::LoadRouteSolution();
// Other
function PageV($aKey=null) {
	$obj = PageVV(2);
	if (!is_null($aKey) && isset($obj->{"$aKey"}))
		return $obj->{"$aKey"};
	return $obj;
}
function PageVA($aKey=null) {
	$obj = PageVV(3);
	if (!is_null($aKey) && isset($obj[$aKey]))
		return $obj[$aKey];
	return $obj;
}
function PageVV($aKey = null) {
	$vv = Hrouter::$VValues;
	if (null === $aKey) return $vv;
	return $vv[$aKey];
}


// DO NOT EDIT BELOW UNLESS YOU KNOW WHAT YOU ARE DOING
// $aAny can be content-type or extension (no dot)
function GetMime($aAny=null) {
	global $MimeListGlobal;
	$aAny = strtolower($aAny);
	if (isset($MimeListGlobal[$aAny]))
		return $MimeListGlobal[$aAny];
	return "";
}

// Main class:
class Hrouter {
	// Variables below are auto-set:
	public static $RouterDir = "";
	public static $URI = "";
	public static $MValues = array();
	public static $DeploySettings = null;
	public static $FileExt = "", $FileName = "";
	public static $LoadPath = "";
	public static $VValues = null;
	protected static $RouteSolution = null;
	// May configure if you want:
	public static $RouteReplacers = array(
		"\[all\]" => "([^\/]+)",
		// URL SAFE latin1:
		"\[usafe\]" => "([a-zA-Z0-9 !\$&'\(\)\+\-\.:;=@\[\]_~]+)",
		// FILE SAFE latin1:
		"\[fsafe\]" => "([a-zA-Z0-9 !\$&'\(\)\+\-\.;=@\[\]_~]+)",
		// name:
		"\[name\]" => "([a-zA-Z0-9']+)",
		"\[fullname\]" => "([a-zA-Z0-9 ']+)",
		// alpha:
		"\[a\]" => "([a-zA-Z]+)",
		// alphanumeric:
		"\[an\]" => "([a-zA-Z0-9]+)",
		// alphanumeric, dot/dash:
		"\[an2\]" => "([a-zA-Z0-9\.]+)",
		"\[an3\]" => "([a-zA-Z0-9\-]+)",
		// ipv6?
		"\[an4\]" => "([a-zA-Z0-9:]+)",
		// number:
		"\[n\]" => "(\d+)",
		// floating number:
		"\[dn\]" => "([0-9\.]+)",
		// username or similar:
		"\[u\]" => "([a-zA-Z0-9\-_\.]+)",
		"\[u2\]" => "([a-zA-Z0-9 '\-_\.]+)",
		// common username:
		"\[u3\]" => "([a-zA-Z0-9_\.]+)",
	);
	// Never call this function outside of this file
	public static function __Main__($aRouterDir) {
		if (CONF_HROUTER_DEPLOY)
			self::DevTools_Deploy($aRouterDir);
		self::$RouterDir = rtrim($aRouterDir, "/");
		self::$DeploySettings = json_decode(file_get_contents(CONF_HROUTER_DEPLOYFILENAME));
		$uri = self::$URI = $GLOBALS["_SERVER"]["REQUEST_URI"];
		// gotta unslash the double or more slashes
		$uri = self::$URI = preg_replace("/(\/\/+)/", "/", $uri);
		$uri = substr($uri, strlen(self::$RouterDir));
		// Etc
		if (CONF_HROUTER_FILES_PATHINFO) {
			$gf = $GLOBALS["_FILES"];
			foreach ($gf as $k=>$v) {
				$GLOBALS["_FILES"][$k]["pathinfo"] = pathinfo($gf[$k]["name"]);
			}
		}
	}
	public static function Install($aAuto=false) {
		// skip if auto and already installed
		if ($aAuto && file_exists(".htaccess"))
			return;
		// etc
		$hrouterFilename = CONF_HROUTER_FULLFILENAME;
		// Write sample page for main inject
		if (!is_dir($modPath = CONF_HROUTER_PATH."/".CONF_HROUTER_MODSMAINDIRNAME)) {
			mkdir(CONF_HROUTER_PATH."/".CONF_HROUTER_MODSMAINDIRNAME);
			mkdir(CONF_HROUTER_PATH."/".CONF_HROUTER_MODSSUBDIRNAME);
			// All pages' mod code sample.
			file_put_contents("$modPath/.php", "<?php
// Here just write library code which will be used by all pages.
// The subfolder \"etc\" is for subpages' scripts, should be same path matching as in \"pages\".
?>");
		}
		// Write sample code for main page
		if (!is_dir(CONF_HROUTER_PATH."/".CONF_HROUTER_PAGESDIRNAME)) {
			mkdir($pagesPath = CONF_HROUTER_PATH."/".CONF_HROUTER_PAGESDIRNAME);
			// Main page code sample.
			file_put_contents("$pagesPath/.php", "<?php\r\n?>");
		}
		$sFilename = CONF_HROUTER_PATH."/.htaccess";
		// Write to file
		file_put_contents($sFilename, "RewriteEngine On\r\nRewriteRule ^(.*?)$ $hrouterFilename [QSA,L]\r\n");
		// Redirect after install
		header("location:.");
		// Need to exit too
		exit;
	}
	// MValue is called to get url match values
	// Example: /path/to/user/[all]/videos/[n]
	// Just print_r() it to figure out how to use it.
	// Supports negative index!
	public static function MValue($aIndex = 0, $aKeyOrIndex2 = null) {
		$c = count(self::$MValues);
		if ($aIndex < 0) $aIndex = $c + $aIndex;
		if ($aIndex < 0) $aIndex = 0;
		if ($aIndex >= $c)
			return null;
		if (!is_null($aKeyOrIndex2)) {
			if ($aKeyOrIndex2 < 0) $aKeyOrIndex2 = $c + $aKeyOrIndex2;
			if ($aKeyOrIndex2 < 0) $aKeyOrIndex2 = 0;
			if ($aKeyOrIndex2 >= $c)
				return null;
			return self::$MValues[$aIndex]->{$aKeyOrIndex2};
		}
		return self::$MValues[$aIndex];
	}
	public static function MValueA($aIndex = 0, $aKeyOrIndex2 = null) { return (array)self::MValue($aIndex, $aKeyOrIndex2); }
	public static function GetPathMatches($aPath = null, &$aOutMValues = null) {
		if (null === $aPath)
			$aPath = self::$URI;
		// Remove trailing slashes
		$aPath = rtrim(explode("?", $aPath)[0], "/");
		// etc
		$matches = array();
		// Loop routes and check
		foreach (self::$DeploySettings->routes as $route) {
			$route = "$route";
			// etc
			$pattern = preg_quote($route, '/');
			foreach (self::$RouteReplacers as $k => $v)
				$pattern = str_replace($k, $v, $pattern);
			$pattern = "/^$pattern\$/";
			// On match append to array
			if (preg_match($pattern, $aPath, $values)) {
				if (null !== $aOutMValues) {
					if (preg_match_all("/\[(.*?)\]/", $route, $values2)) {
						$keyA = $values2[1];
						$values[0] = $route;
						for ($i = 0, $c = -1 + count($values); $i < $c; ++$i) {
							$vi = 1 + $i;
							$key = $keyA[$i];
							if (isset($values[$key])) {
								$values[$key][] = $values[$vi];
							} else $values[$key] = array($values[$vi]);
						}
					}
					$aOutMValues[] = (object)$values;
				}
				$matches[] = $route;
			}
		}
		return $matches;
	}
	// Removes filename from path
	protected static function RemoveFilename($aPath = null, &$aOutFileName = null) {
		if (null === $aPath)
			$aPath = self::$URI;
		$pathlen = strlen($aPath);
		$parts = explode("/", $aPath);
		$partsc = count($parts);
		if ($partsc) {
			$lastpart = $parts[-1 + $partsc];
			$aOutFileName = explode("?", $lastpart)[0];
			$lpslen = strlen($lastpart);
			if (0 < $lpslen)
				return substr($aPath, 0, $pathlen-$lpslen-1);
		}
		return $aPath;
	}
	// Returns array, for action to be performed
	public static function FindRouteSolution($aURI = null, $aSetSolution = true) {
		if (null === $aURI)
			$aURI = self::$URI;
		$routeSolution = null;
		$aURI = urldecode(explode("?", $aURI)[0]);
		// Remove last slashes if configured
		if (CONF_HROUTER_RMLS)
			$aURI = rtrim($aURI, "/");
		// Etc
		$ext = GetFileExt($aURI);
		$virtPath = substr($aURI, self::$DeploySettings->rmBeginLen);
		// set filename/fileExt
		self::$FileExt = $ext;
		self::RemoveFilename($virtPath, self::$FileName);
		// Basic matching
		$matches = self::GetPathMatches($virtPath, self::$MValues);
		$matchesc = 0;
		// Default paths for fetching page
		$loadPath = self::$DeploySettings->pagesDirName.$virtPath;
		$mLoadPathMain = self::$DeploySettings->modsMainDirName;
		$mLoadPathSub = self::$DeploySettings->modsSubDirName.$virtPath;
		if ($matchesc = count($matches)) {
			if (!in_array($aURI, $matches)) {
				$aURI = $matches[0];
				$mLoadPathSub = self::$DeploySettings->modsSubDirName.$aURI;
				$loadPath = self::$DeploySettings->pagesDirName.$aURI;
			}
		}
		self::$LoadPath = $loadPath;
		// Pass it
		if ($ext && is_file("$loadPath")) {
			$routeSolution = array("file", $loadPath, $ext);
		} else if (file_exists($ifn = "$loadPath/.php") || file_exists($ifn = "$loadPath/page.php")) {
			$routeSolution = array("page", $ifn, "$mLoadPathMain/.php", "$mLoadPathSub/.php");
		} else {
			$rfn = self::RemoveFilename($virtPath, $filename);
			$matches = self::GetPathMatches("$rfn", self::$MValues);
			$matchesc = count($matches);
			if ($matchesc) {
				$rpath = $matches[0];
				$rfp = self::$DeploySettings->pagesDirName."$rpath/$filename";
				$routeSolution = array("file", $rfp, $ext);
			}
		}
		if (!$routeSolution)
			$routeSolution = array(404);
		if ($aSetSolution)
			self::$RouteSolution = $routeSolution;
		self::$VValues = array(
			"0" => $filename = self::$FileName,
			"FileName" => &$filename,
			"1" => $ext = self::$FileExt,
			"FileExt" => &$ext,
			"2" => $obj = self::MValue(),
			"V" => &$obj,
			"3" => $arr = (array)$obj,
			"VA" => &$arr,
			"4" => $vf = self::MValue(0, 0),
			"VPath" => &$vf,
			"5" => $lp = self::$LoadPath,
			"RPath" => &$lp,
			"6" => $reqp = self::$URI,
			"Path" => &$reqp
		);
		return $routeSolution;
	}
	// Loads the actual page/file.
	public static function LoadRouteSolution() {
		$solution = self::$RouteSolution;
		if (404 === $solution[0]) {
			Render404();
		} else if ("file" === $solution[0]) {
			RenderFile($solution[1], $solution[2]);
		} else if ("page" === $solution[0]) {
			// Import main
			if (is_file($imfn = $solution[2]))
				require_once $imfn;
			// Then import sub
			if (file_exists($ifn2 = $solution[3]))
				require_once $ifn2;
			// Lastly load page
			require_once $solution[1];
		}
		exit;
	}
	// Function name is subject to change, avoid using!
	public static function DevTools_Deploy($aPath) {
		$pagesDirName = CONF_HROUTER_PAGESDIRNAME;
		$modsMainDirName = CONF_HROUTER_MODSMAINDIRNAME;
		$modsSubDirName = CONF_HROUTER_MODSSUBDIRNAME;
		$docRoot = $GLOBALS["_SERVER"]["DOCUMENT_ROOT"];
		$hrpath = str_replace("\\", "/", trim($aPath, "/\\"));
		$hrpath2 = substr($hrpath, strlen($docRoot));
		$pagesDir = "$hrpath/$pagesDirName";
		$dirs = RecursiveGetDirs($pagesDir);
		$pathn = explode("/", $hrpath);
		$pathn = $pathn[-1 + count($pathn)];
		// To not bother with long loading when deploying:
		if (86400 < time() - @filemtime(CONF_HROUTER_DEPLOYMIMESFILENAME)) {
			$phpStr = self::DevTools_GenMimesCode(null, $fetchFail);
			if (!$fetchFail)
				file_put_contents(CONF_HROUTER_DEPLOYMIMESFILENAME, $phpStr);
		}
		// Save deploy json:
		// Updates on call
		file_put_contents(CONF_HROUTER_DEPLOYFILENAME, json_encode(array(
			"pagesDirName" => $pagesDirName,
			"modsMainDirName" => $modsMainDirName,
			"modsSubDirName" => $modsSubDirName,
			"docRoot" => $docRoot,
			"mainDir" => $hrpath2,
			"rmBeginLen" => strlen("$hrpath2"),
			"pagesDir" => $pagesDir,
			"routes" => $dirs
		), 1*128));
	}
	// Function name is subject to change, avoid using!
	public static function DevTools_GenMimesCode($aApacheMimeURL = null, &$aFail=false) {
		if (null === $aApacheMimeURL)
			$aApacheMimeURL = "http://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types";
		$lines = explode("\n", strval(@file_get_contents($aApacheMimeURL)));
		$ret = array();
		$i = 0;
		if (1 > count($lines))
			return $aFail=true;
		foreach($lines as $line) {
			$line = trim($line);
			if ($line && "#" == $line[0])
				continue;
			$match = preg_split("/\t+/", $line);
			if (false === $match || 2 > count($match))
				continue;
			// $match[0=mimetype, 1=ext]
			$exts = preg_split("/\s+/", $match[1]);
			$ret[$match[0]] = $exts;
			foreach ($exts as $ext)
				$ret[$ext] = $match[0];
		}
		return "<?php\r\n\$MimeListGlobal = ".TrimLines(var_export($ret,1))."\r\n?>";
	}
}

function TrimLines($aStr) {
	return preg_replace("/^\\s+|\\s+\$/m", "", $aStr);
}

// All params of BaseHref() can be null, or empty string:
// $aHref can be any false value to set as default value.
// if $aAfterStr is false value, and not empty, it defaults to: \r\n
function BaseHref($aHref = "", $aBeforeStr = null, $aAfterStr = null, $aUseReturn = false) {
	// <base href> works in IE6, yes.
	$href = $aHref;
	if (!$aAfterStr && "" !== $aAfterStr)
		$aAfterStr = "\r\n";
	// etc
	if (!$href) {
		$href = rtrim(explode("?", $GLOBALS["_SERVER"]["REQUEST_URI"])[0], "/");
	}
	$ctx = "$aBeforeStr<base href=\"$href/\" /><!--[if IE]></base><![endif]-->$aAfterStr";
	if ($aUseReturn) return $ctx;
	echo $ctx;
}
function Render404($aContent = null, $aExit = true) {
	// Display if set:
	if ($aContent)
		echo $aContent;
	else if (CONF_HROUTER_DEBUG)
		echo "404; Not found.";
	// Set error code:
	if (function_exists("http_response_code"))
		http_response_code(404);
	if ($aExit) exit;
}
// Can use it to raw video/audio/image/file, and CAN RESUME if pause:
function RenderFile($aFilename, $aExt) {
	if (!file_exists($aFilename)) {
		return Render404(null);
	}
	// Ensure $aExt is lowercase:
	$aExt = strtolower("$aExt");
	// Better content-type be nothing at first:
	header("content-type:");
	// Handle rest file extensions
	if ("php" === $aExt) {
		return Render404(null);
	} else header("content-type: ".GetMime($aExt));
	// Render file contents
	$pi = pathinfo($aFilename);
	ServeFile($aFilename, $pi["basename"], 0, true, null);
	exit;
}
// Can use it to download/raw file, and CAN RESUME if pause:
function ServeFile($aPathToFile, $aFileName = null, $aMaxSpeedKiB = 200000, $aStream = false, $aContentType = "", $aExit = true) {
	if (!file_exists($aPathToFile) || !is_readable($aPathToFile))
		return false;
	// Remove troublesome headers
	header_remove("Cache-Control");
	header_remove("Pragma");
	header("Expires: Fri, 13 Dec 1901 20:45:52 GMT");
	// Default to send entire file
	$startPos = 0;
	$byteLength = $fileLength = filesize($aPathToFile);
	$maxSpeed = $aMaxSpeedKiB * 1024;
	if (0 === $byteLength)
		exit;
	if (1 > $maxSpeed)
		$maxSpeed = 2147483647;
	// Remove float:
	$maxSpeed = (int)$maxSpeed;
	// Etc
	header("Accept-Ranges: bytes");
	if ($aContentType)
		header("Content-Type: $aContentType");
	// Etc
	if ($aFileName) {
		// Streamingness
		$contentDisp = "attachment";
		if ($aStream)
			$contentDisp = "inline";
		// Handle dots anyway
		$aFileName = preg_replace("/\./", "%2E", $aFileName, substr_count($aFileName, '.') - 1);
		header("Content-Disposition: $contentDisp; filename=\"$aFileName\"");
	}
	// More tweaks
	if (strstr($_SERVER['HTTP_USER_AGENT'], "MSIE"))
		if (ini_get('zlib.output_compression')) {
			ini_set('zlib.output_compression', 'Off');
		}
	// Parse range, eg: "bytes=12345-", "bytes=12345-67890"
	if (isset($_SERVER['HTTP_RANGE']) && preg_match('%bytes=(\d+)-(\d+)?%i', $_SERVER['HTTP_RANGE'], $match)) {
		// Read from offset
		$startPos = (int)$match[1];
		// Get length
		if(isset($match[2])){
			$endPos = (int)$match[2];
			$byteLength = 1 + $endPos;
		} else {
			$endPos = -1 + $fileLength;
		}
		// Omit only if HTTP_RANGE is requested
		header("HTTP/1.1 206 Partial content");
		header("Content-Range: bytes $startPos-$endPos/$fileLength");
	}
	// Etc
	$byteRange = 0;
	if (CONF_HROUTER_USE_OBLEN)
		$byteRange = ob_get_length();
	$byteRange += $byteLength - $startPos;
	header("Content-Length: $byteRange");
	// Etc
	$buf = "";
	$bufSize = 8192;
	// File cannot be opened?
	if (!($handle = fopen($aPathToFile, "r")))
		return false;
	// Offset not found?
	if (-1 === fseek($handle, $startPos, SEEK_SET))
		return false;
	// Read file:
	$bytesWritten = 0;
	while (!feof($handle)) {
		$bufSizeTmp = min($bufSize, $byteRange);
		// Try read by $bufSizeTmp bytes
		$buf = fread($handle, $bufSizeTmp);
		// Store how many bytes were actually read
		$bufSizeTmp = strlen($buf);
		// Break if no bytes to read
		if (1 > $bufSizeTmp)
			break;
		// Increment
		$bytesWritten += $bufSizeTmp;
		// Write the buf to output
		echo $buf;
		flush();
		// Limiter?
		if (0 === $bytesWritten % $maxSpeed)
			sleep(1);
	}
	if ($aExit) exit;
}
// Gets extension from path/uri, ignoring query: ?
// Returns only extension, no dot
function GetFileExt($aUri) {
	// slash separate
	$ssep = explode("/", $aUri);
	// skip to read after last slash
	if (($ssepc = count($ssep)))
		$aUri = $ssep[-1 + $ssepc];
	// read after last slash
	$dsep = explode(".", explode("?", $aUri)[0]);
	$dsepc = count($dsep);
	if ($dsepc > 1) {
		$ext = strtolower($dsep[-1 + $dsepc]);
		return $ext;
	}
	return "";
}

// $aInitDirLen will ONLY be used IF true is $aReturnUnrealPath
// $aInitDirLen will be AUTO SET IF true is $aReturnUnrealPath
function RecursiveGetDirs($aStartDir, $aReturnUnrealPath = true, $aInitDirLen = 0, $aFirstCall = true) {
	$ret = array();
	if ($aFirstCall) {
		if ($aReturnUnrealPath) {
			$ret[] = "/";
			$aInitDirLen = strlen($aStartDir);
		} else $ret[] = $aStartDir;
	}
	$dr = scandir("$aStartDir");
	if ($dr) foreach ($dr as $name) {
		if ("." === $name || ".." === $name)
			continue;
		if (is_file($name = "$aStartDir/$name"))
			continue;
		if ($aReturnUnrealPath)
			$ret[] = substr($name, $aInitDirLen);
		else $ret[] = $name;
		$ret = array_merge($ret, RecursiveGetDirs($name, $aReturnUnrealPath, $aInitDirLen, false));
	}
	return $ret;
}


// Utilities

function SStart($aSessionName = "sessid") {
	session_name($aSessionName);
	session_start();
}

function GVar($aN, $aAbbr="") {
	// $aAbbr: s for session, S for server
	if (!$aAbbr) $aAbbr = "";
	$g = $GLOBALS;
	$da = strtolower($aAbbr);
	if ("g" === $da) return $g["_GET"];
	else if ("p" === $da) return $g["_POST"];
	else if ("f" === $da) return $g["_FILES"];
	else if ("s" === $aAbbr) return $g["_SESSION"];
	else if ("S" === $aAbbr) return $g["_SERVER"];
	else if ("c" === $da) return $g["_COOKIE"];
	else if ("r" === $da) return $g["_REQUEST"];
	else if ("e" === $da) return $g["_ENV"];
	return $g[$aN];
}
function ZGet(&$aOutVar) {
	// Reset value, in case of reuse:
	$aOutVar = array();
	foreach (array_slice(func_get_args(), 1) as $arg) {
		$varAbbr = $arg[0];
		$paramName = substr($arg, 1);
		$cVar = GVar(0, $varAbbr);
		if (isset($cVar[$paramName]))
			$aOutVar[] = $cVar[$paramName];
		else return false;
	}
	return true;
}

function XXGet(&$aVar, &$aOutVar = null, $aParams = null) {
	if (!is_array($aParams))
		return true;
	// Reset value, in case of reuse:
	$aOutVar = array();
	// etc
	$args = $aParams;
	if (isset($aParams[0]) && is_array($aParams[0]))
		$args = $aParams[0];
	// Check them all
	foreach ($args as $arg) {
		if (isset($aVar[$arg])) {
			$aOutVar[] = $aVar[$arg];
		} else return false;
	}
	return true;
}

function XXHas(&$aVar, $aParams = null) {
	if (!is_array($aParams))
		return true;
	$args = $aParams;
	if (isset($aParams[0]) && is_array($aParams[0]))
		$args = $aParams[0];
	foreach ($args as $arg)
		if (!isset($aVar[$arg]))
			return false;
	return true;
}

function XXSet(&$aVar, $aParams = null) {
	// Set them all
	$args = null;
	if (is_array($aParams))
		$args = $aParams;
	else $args = array_slice(func_get_args(), 1);
	// etc
	if (IsArrayList($args)) {
		for ($i = 0, $c = count($args); $i < $c; $i += 2) {
			$k = $args[0 + $i];
			$v = $args[1 + $i];
			$aVar[$k] = $v;
		}
	} else foreach ($args as $k=>$v)
		$aVar[$k] = $v;
}


/*

// Example for "Unsets" function:
Unsets($_SESSION, "a", "b");

*/
function Unsets(&$aVar, $aParams = null) {
	$args = $aParams;
	if (!is_array($aParams))
		$args = array_slice(func_get_args(), 1);
	foreach ($args as $v) {
		if (isset($aVar[$v]))
			unset($aVar[$v]);
	}
}


/*
// Sample for ZGet()
// Each param name has prefix of each type, p for $_POST, g for $_GET ...
if (ZGet($values, "ggetParam1", "ppostParam1")) {
	// Assign $a as $_GET["getParam1"]
	// Assign $b as $_POST["postParam1"]
	list($a, $b) = $values;
}
*/


/*
// Sample for GGet, other (except ZGet) functions use same syntax:
if (GGet($gValues, "getParam1", "getParam2")) {
	// Assign $a, $b with $_GET values:
	list($a, $b) = $gValues;
}
*/


/*
// Sample for GHas, other (XHas) functions use same syntax:
if (GHas("getParam1", "getParam2")) {
	// do whatever else:
}
*/

// Havers
function GHas($aOptionalArray = null) {
	return XXHas($GLOBALS["_GET"], func_get_args());
}

function PHas($aOptionalArray = null) {
	return XXHas($GLOBALS["_POST"], func_get_args());
}

function RHas($aOptionalArray = null) {
	return XXHas($GLOBALS["_REQUEST"], func_get_args());
}

function FHas($aOptionalArray = null) {
	return XXHas($GLOBALS["_FILES"], func_get_args());
}

function SHas($aOptionalArray = null) {
	return XXHas($GLOBALS["_SESSION"], func_get_args());
}

function CHas($aOptionalArray = null) {
	return XXHas($GLOBALS["_COOKIE"], func_get_args());
}

// Getters
function GGet(&$aOutVar, $aOptionalArray = null) {
	return XXGet($GLOBALS["_GET"], $aOutVar, array_slice(func_get_args(), 1));
}

function PGet(&$aOutVar, $aOptionalArray = null) {
	return XXGet($GLOBALS["_POST"], $aOutVar, array_slice(func_get_args(), 1));
}

function RGet(&$aOutVar, $aOptionalArray = null) {
	return XXGet($GLOBALS["_REQUEST"], $aOutVar, array_slice(func_get_args(), 1));
}

function FGet(&$aOutVar, $aOptionalArray = null) {
	return XXGet($GLOBALS["_FILES"], $aOutVar, array_slice(func_get_args(), 1));
}

function SGet(&$aOutVar, $aOptionalArray = null) {
	return XXGet($GLOBALS["_SESSION"], $aOutVar, array_slice(func_get_args(), 1));
}

function CGet(&$aOutVar, $aOptionalArray = null) {
	return XXGet($GLOBALS["_COOKIE"], $aOutVar, array_slice(func_get_args(), 1));
}


// Setters

// Sample: SSet( "key1", "value1",  "key2", "value2" );
// Sample: SSet( array( "key1","value1",  "key2","value2" ) );
// Sample: SSet( array( "key1"=>"value1",  "key2"=>"value2" ) );
function SSet($aParams = null) {
	if (!is_array($aParams))
		$aParams = func_get_args();
	return XXSet($GLOBALS["_SESSION"], $aParams);
}

// Unsetters

// Sample: SUnset("a","b")
// Sample: SUnset( array("a","b") );
function SUnset($aParams = null) {
	if (!is_array($aParams))
		$aParams = func_get_args();
	return Unsets($GLOBALS["_SESSION"], $aParams);
}

// Etc
function AuthBasic(&$aOutUser, &$aOutPw, $aForceAuth = false) {
	$cVar = &$GLOBALS["_SERVER"];
	if (!isset($cVar["PHP_AUTH_USER"]) || $aForceAuth) {
		header("WWW-Authenticate: Basic");
		return false;
	}
	$aOutUser = $cVar["PHP_AUTH_USER"];
	$aOutPw = $cVar["PHP_AUTH_PW"];
	return true;
}

// if $aAfterStr is false value, and not empty, it defaults to: \r\n
function HtmlJsonPre($aCtx, $aOptions=128, $aBeforeStr=null, $aAfterStr=null, $aEcho=true) {
	$aCtx = json_encode($aCtx, $aOptions);
	$html = HtmlPre($aCtx, $aBeforeStr, $aAfterStr, 0);
	if ($aEcho)
		echo $html;
	return $html;
}
// if $aAfterStr is false value, and not empty, it defaults to: \r\n
function HtmlPre($aCtx, $aBeforeStr=null, $aAfterStr=null, $aEcho=true) {
	if (!$aAfterStr && "" !== $aAfterStr)
		$aAfterStr = "\r\n";
	// etc
	$html = "$aBeforeStr<pre>$aCtx</pre>$aAfterStr";
	if ($aEcho)
		echo $html;
	return $html;
}

function JsonGet() {
	return json_decode(file_get_contents("php://input"));
}

function InputGet() {
	return file_get_contents("php://input");
}

// Returns true if empty, or all keys are number.
// Only use if necessary.
function IsArrayList($aArr) {
	$i = 0;
	foreach ($aArr as $k => $v)
		if ($i++ !== $k)
			return!1;
	return!0;
}
?>
